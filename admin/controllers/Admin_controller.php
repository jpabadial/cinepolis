<?php

class Admin_controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->admins = Admins_bl::listarAdmins();
      $this->view->render($this,"index","Admins");
      }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

  public function update(){
      $admin=$_POST;
      $r = Admins_bl::update($admin);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
      $this->view->admins=Admin::where("id",$id);
      $this->view->render($this,"editar","Editar Categorias");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = Admins_bl::delete($id);
    header("Location:".URL."Admin/index");

  }

  public function save(){
      $this->validateKeys(["username","password"], $_POST);
      $admin = $_POST;
      $r = Admins_bl::save($admin);
      print_r($admin);
      //$this->view->render($this,"print","prueba");
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->render($this,"crear","Crear Administrador");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
