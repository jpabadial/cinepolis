<?php

class CategoriaPelicula_controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->categorias = Categoria_bl::listarCategorias();
      $this->view->render($this,"index","Funciones");
      }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

  public function update(){
      $categorias=$_POST;
      $r = categoria_bl::update($categorias);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
      $this->view->categorias=CategoriaPelicula::where("id",$id);
      $this->view->render($this,"editar","Editar Categorias");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = Categoria_bl::delete($id);
    header("Location:".URL."CategoriaPelicula/index");

  }

  public function save(){
      $this->validateKeys(["nombre"], $_POST);
      $categoria = $_POST;
      $r = Categoria_bl::save($categoria);
      //$this->view->render($this,"print","prueba");
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->render($this,"crear","Categoria_bl");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
