<?php

class Ciudad_controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->ciudades = Ciudad_bl::listarTodasCiudades();
       $this->view->paises = Pais_bl::listarPaises();
       //print_r( $this->view->ciudades);
      $this->view->render($this,"index","Ciudades");
      }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

  public function update(){
      $ciudad=$_POST;
      $r = Ciudad_bl::update($ciudad);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
      $this->view->admin = Admin::getById(Session::get("admin_id"));
      $this->view->paises = Pais_bl::listarPaises();
      $this->view->ciudad=Ciudad::where("id",$id);
      //print_r($this->view->ciudad);
      $this->view->render($this,"editar","Editar ciudades");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = Ciudad_bl::delete($id);
    header("Location:".URL."Ciudad/index");

  }

  public function save(){
      $this->validateKeys(["nombre","pais_id"], $_POST);
      $ciudad = $_POST;
      $r = Ciudad_bl::save($ciudad);
      print_r($r);
      //$this->view->render($this,"print","prueba");
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->paises = Pais_bl::listarPaises();
       $this->view->render($this,"crear","Crear una nueva ciudad");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
