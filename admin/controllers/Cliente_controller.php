<?php

class Cliente_controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->clientes = Cliente_bl::listarClientes();
      $this->view->render($this,"index","Clientes");
      }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

  public function update(){

      $cliente=$_POST;
        //print_r($cliente);
      $r = Cliente_bl::update($cliente);
      print_r($cliente);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
      //$this->view->clientes = Cliente_bl::listarClientes();
      $this->view->cliente=Cliente::where("id",$id);
      $this->view->render($this,"editar","Editar Clientes");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = Cliente_bl::delete($id);
    header("Location:".URL."Cliente/index");

  }

  public function save(){
      $this->validateKeys(["nombre","contrasena","email","fechaNacimiento","telefono"], $_POST);
      $cliente = $_POST;

      if($cliente["identificacion"] == null){
        $cliente["identificacion"] = 0;
      }
      print_r($cliente);
      $r = Cliente_bl::save($cliente);
      print_r($r);

      //$this->view->render($this,"print","prueba");
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->render($this,"crear","Cliente");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
