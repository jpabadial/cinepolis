<?php

class Funcion_controller extends Controller {

    public function index(){

      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
          $this->view->funciones=Funcion_bl::listarFunciones();
          $this->view->peliculas=Pelicula::getAll();
          $this->view->salas=Sala::getAll();
          $this->view->teatros=Teatro::getAll();
          $this->view->render($this,"index","Funciones");
      }
       else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

    public function crear(){
      if(Session::get('admin_id')){
         $this->view->admin = Admin::getById(Session::get("admin_id"));
          $this->view->salas=Sala::getAll();
         $this->view->teatros=Teatro::getAll();
           $this->view->peliculas=Pelicula::getAll();
         $this->view->render($this,"crear","Funciones");

      }
       else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
    }



    public function editar($id){
      if(Session::get('admin_id')){
         $this->view->admin = Admin::getById(Session::get("admin_id"));
        $this->view->funciones=Funcion::where("id",$id);
         $this->view->salas=Sala::getAll();
        $this->view->teatros=Teatro::getAll();
          $this->view->peliculas=Pelicula::getAll();
        $this->view->render($this,"editar","Editar Funciones");

      }
       else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
    }

  public function update(){
      $funciones=$_POST;
      $r = Funcion_bl::update($funciones);
      header("Location:index");
  }

  public function eliminar($id){
    $r = Funcion_bl::delete($id);
   header("Location:".URL."Funcion/index");

  }




    public function save(){
        $this->validateKeys(["sala","pelicula","fecha","hora"], $_POST);
        $funcion = $_POST;

        $r = Funcion_bl::save($funcion);


        $this->view->render($this,"print","prueba");

        header("Location:index");
    }


}
