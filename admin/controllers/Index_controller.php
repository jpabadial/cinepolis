<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Index_controller extends Controller{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {


     if(Session::get('admin_id')){
       $date= date('Y-m-d');
       //$date= 'now()';
          $this->view->admin = Admin::getById(Session::get("admin_id"));
          $this->view->funciones=Funcion::where("fecha",$date);
          $this->view->peliculas=Pelicula::getAll();
          $this->view->salas=Sala::getAll();
          $this->view->teatros=Teatro::getAll();



      $this->view->render($this,"index","Cinepolis Admin Panel");
     }else{

        $this->view->render($this,"login","Cinepolis Admin Panel");
     }


    }

}
