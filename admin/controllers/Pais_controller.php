<?php

class Pais_Controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->paises = Pais_bl::listarPaises();
      $this->view->render($this,"index","Funciones");
      }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

  public function update(){
      $categorias=$_POST;
      $r = Pais_bl::update($categorias);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
      $this->view->paises=Pais::where("id",$id);
      $this->view->render($this,"editar","Editar Paises");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = Pais_bl::delete($id);
    header("Location:".URL."Pais/index");

  }

  public function save(){
      $this->validateKeys(["nombre"], $_POST);
      $pais = $_POST;
      $r = Pais_bl::save($pais);
      //$this->view->render($this,"print","prueba");
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->render($this,"crear","Pais_bl");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
