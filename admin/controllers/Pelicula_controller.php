<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pelicula_controller
 *
 * @author pabhoz
 */
class Pelicula_controller extends Controller {

    public function index(){

      if(Session::get('admin_id')){
        $this->view->admin = Admin::getById(Session::get("admin_id"));
        $this->view->peliculas = Peliculas_bl::Peliculas();

        $this->view->categorias =CategoriaPelicula::getAll();
        $this->view->render($this,"index","Pelicula");
      }else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

    public function crear(){
      if(Session::get('admin_id')){
         $this->view->admin = Admin::getById(Session::get("admin_id"));
         $this->view->categorias = CategoriaPelicula::getAll();
         $this->view->render($this,"crear","Crear Pelicula");
      }
       else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
    }

    public function editar($id){
      if(Session::get('admin_id')){
         $this->view->admin = Admin::getById(Session::get("admin_id"));
          $this->view->peliculas = Pelicula::where("id",$id);

         $this->view->categorias = CategoriaPelicula::getAll();
         $this->view->render($this,"editar","Editar Pelicula");
      }
       else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
    }

  public function update(){
      $pelicula=$_POST;

      if ($_FILES["poster"]["size"]>0) {
        $r = Peliculas_bl::update($pelicula,$_FILES["poster"]);
            header("Location:index");
      }else {
        $r = Peliculas_bl::update($pelicula);
        header("Location:index");
      }
  }

    public function eliminar($id){

      $r = Peliculas_bl::delete($id);

      if($r==0){
          header("Location:".URL."Pelicula/indexe");

      }else{
          header("Location:".URL."Pelicula/index");
      }





    }


    public function save(){

        $this->validateKeys(["titulo","sinopsis","fecha_estreno","categoria","sinopsis"], $_POST);
        $pelicula = $_POST;
        $r = Peliculas_bl::save($pelicula,$_FILES["poster"]);
        header("Location:index");
    }

   public function indexe(){
        echo "<script type=\"text/javascript\">alert(\"Lo sientimos hubo un error en el proceso\");</script>";
        if(Session::get('admin_id')){
          $this->view->admin = Admin::getById(Session::get("admin_id"));
          $this->view->peliculas = Peliculas_bl::Peliculas();

          $this->view->categorias =CategoriaPelicula::getAll();
          $this->view->render($this,"index","Pelicula");
        }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
        }
    }

}
