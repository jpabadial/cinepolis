<?php

class Reserva_controller extends Controller {

    public function index(){

      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
          $this->view->funciones=Funcion_bl::listarFunciones();
          $this->view->reservas=Reserva_bl::listarReservas();
          $this->view->clientes=Cliente_bl::listarClientes();
          $this->view->peliculas= Peliculas_bl::Peliculas();
          $this->view->render($this,"index","Reservas");
      }
       else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

    public function crear(){
      if(Session::get('admin_id')){
         $this->view->admin = Admin::getById(Session::get("admin_id"));
         $this->view->funciones=Funcion_bl::listarFunciones();
         $this->view->clientes=Cliente_bl::listarClientes();
        $this->view->peliculas= Peliculas_bl::Peliculas();
         $this->view->render($this,"crear","Reserva");

      }
       else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
    }



    public function editar($id){
      if(Session::get('admin_id')){
         $this->view->admin = Admin::getById(Session::get("admin_id"));
        $this->view->reserva=Reserva::where("id",$id);
        $this->view->funciones=Funcion_bl::listarFunciones();
        $this->view->clientes=Cliente_bl::listarClientes();
       $this->view->peliculas= Peliculas_bl::Peliculas();
        $this->view->render($this,"editar","Editar Reservas");

      }
       else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
    }

  public function update(){

      $reserva=$_POST;

      $reserva["precio"] = $reserva["numSillas"]*8000;
      unset($reserva["numSillas"]);
      //print_r($reserva);
      $r = Reserva_bl::update($reserva);

      //print_r($r);
      header("Location:index");
  }

  public function eliminar($id){
    $r = Reserva_bl::delete($id);
    print_r($r);
   header("Location:".URL."Reserva/index");

  }

    public function save(){
        $this->validateKeys(["forma_de_pago","estado_pago","numSillas","cliente_id","funcion_id"], $_POST);
        $reservaArr = $_POST;

        $reservaArr["precio"] = $reservaArr["numSillas"]*8000;
        unset($reservaArr["numSillas"]);

        //print_r($reservaArr);

      $r = Reserva_bl::saveReservaAdmin($reservaArr);

    //  print_r($r);
        header("Location:index");
    }


}
