<?php

class Sala_controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->salas = Sala_bl::listarSalas();
       $this->view->tiposSala = TipoSala_bl::listarTipoSala();
       $this->view->teatros = Teatro_bl::allTeatros();
      // print_r( $this->view->ciudades);
      $this->view->render($this,"index","Salas");
      }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

  public function update(){
      $sala=$_POST;
      print_r($_POST);
      $r = Sala_bl::update($sala);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
      $this->view->admin = Admin::getById(Session::get("admin_id"));
      $this->view->tiposSala = TipoSala_bl::listarTipoSala();
      $this->view->teatros = Teatro_bl::allTeatros();
      $this->view->sala=Sala::where("id",$id);
      //print_r($this->view->ciudad);
      $this->view->render($this,"editar","Editar Teatro");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = Sala_bl::delete($id);
    header("Location:".URL."Sala/index");

  }

  public function save(){
      $this->validateKeys(["numero","capacidad","teatro_id","tipo_id"], $_POST);
      $sala = $_POST;
      $r = Sala_bl::save($sala);
      print_r($r);
      //$this->view->render($this,"print","prueba");
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->tiposSala = TipoSala_bl::listarTipoSala();
       $this->view->teatros = Teatro_bl::allTeatros();
       $this->view->render($this,"crear","Crear una nueva Sala");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
