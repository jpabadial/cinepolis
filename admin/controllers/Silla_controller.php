<?php

class Silla_controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->salas = Sala_bl::listarSalas();
      // print_r($this->view->salas);
       $this->view->sillas = Silla_bl::listarSillas();
      //  print_r($this->view->sillas);
      // print_r( $this->view->ciudades);
      $this->view->render($this,"index","Sillas");
      }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

  public function update(){
      $silla=$_POST;
      print_r($_POST);
      $r = Silla_bl::update($silla);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
      $this->view->admin = Admin::getById(Session::get("admin_id"));
      $this->view->salas = Sala_bl::listarSalas();
      $this->view->sillas = Silla_bl::listarSillas();
      $this->view->silla=Silla::where("id",$id);
      //print_r($this->view->silla);
      $this->view->render($this,"editar","Editar Silla");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = Silla_bl::delete($id);
    header("Location:".URL."Silla/index");

  }

  public function save(){
      $this->validateKeys(["numero","fila","estado","sala_id"], $_POST);
      $silla = $_POST;
      $r = Silla_bl::save($silla);
      //print_r($sala);
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->salas = Sala_bl::listarSalas();
       $this->view->sillas = Silla_bl::listarSillas();
       $this->view->render($this,"crear","Crear una nueva Sala");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
