<?php

class Teatro_controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->ciudades = Ciudad_bl::listarTodasCiudades();
       $this->view->teatros = Teatro_bl::allTeatros();
      // print_r( $this->view->ciudades);
      $this->view->render($this,"index","Teatros");
      }else{
          $this->view->render($this,"login","Cinepolis Admin Panel");
       }
  }

  public function update(){
      $teatro=$_POST;
      print_r($_POST);
      $r = Teatro_bl::update($teatro);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
      $this->view->admin = Admin::getById(Session::get("admin_id"));
      $this->view->ciudades = Ciudad_bl::listarTodasCiudades();
      $this->view->teatro=Teatro::where("id",$id);
      //print_r($this->view->ciudad);
      $this->view->render($this,"editar","Editar Teatro");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = Teatro_bl::delete($id);
    header("Location:".URL."Teatro/index");

  }

  public function save(){
      $this->validateKeys(["nombre","telefono","direccion","ciudad_id"], $_POST);
      $teatro = $_POST;
      $r = Teatro_bl::save($teatro);
      print_r($r);
      //$this->view->render($this,"print","prueba");
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->ciudades = Ciudad_bl::listarTodasCiudades();
       $this->view->render($this,"crear","Crear una nueva ciudad");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
