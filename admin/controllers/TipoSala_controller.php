<?php

class TipoSala_controller extends Controller {

    public function index(){
      if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->tipoSalas = TipoSala_bl::listarTipoSala();
      $this->view->render($this,"index","Tipo Sala");

      }else{
          $this->view->render($this,"index","Cinepolis Admin Panel");
       }
  }

  public function update(){
      $tipoSala=$_POST;
      $r = TipoSala_bl::update($tipoSala);
      header("Location:index");
  }

  public function editar($id){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
      $this->view->tipoSalas=TipoSala::where("id",$id);
      $this->view->render($this,"editar","Editar Tipo de la sala");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }

  public function eliminar($id){
    $r = TipoSala_bl::delete($id);
    header("Location:".URL."TipoSala/index");

  }

  public function save(){
      $this->validateKeys(["nombre"], $_POST);
      $tipoSala = $_POST;
      $r = TipoSala_bl::save($tipoSala);
      //$this->view->render($this,"print","prueba");
      header("Location:index");
  }

  public function crear(){
    if(Session::get('admin_id')){
       $this->view->admin = Admin::getById(Session::get("admin_id"));
       $this->view->render($this,"crear"," Crear nuevos tipos de la sala");

    }
     else{
        $this->view->render($this,"login","Cinepolis Admin Panel");
     }
  }


}
