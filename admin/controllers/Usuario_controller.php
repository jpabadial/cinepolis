<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Usuario_controller extends Controller{

    function __construct() {
        parent::__construct();
    }

    public function login()
    {
        $this->validateKeys(["username","password"], $_POST);
        $r = Admins_bl::login($_POST["username"],$_POST["password"]);

        if($r){
            $adm = Admin::getBy("username", $_POST["username"]);
            Session::set("admin_id", $adm->getId());
            header("Location:".URL);
        }
    }
    public function logout()
    {
        Session::remove("admin_id");
        Session::destroy();
        header("Location:".URL);
    }

}
