<?php include MODULE."head.php"; ?>

<body style="background-color: #00587A;">
    <?php include MODULE."headerC.php"; ?>

    <div class="row">
				<div class="margen_top_login"></div>
	<div class="col-xs-0 col-sm-0 col-md-1 col-lg-1"></div>
	<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="panel-heading content-center">
      <div class="pull-left" style="margin-bottom:10px; ">
      <a class="btn btn-default" style="width:200%; color: #00587A; font-weight:bolder;" href="<?php echo URL;?>Admin/crear">Agregar Administrador <span class=""></span></a>
      </div>
    </div>
    <br>
    <br>
		<div class="panel panel-default" style="  border-color: #59A9C2;  border-style: solid;  border-width: 0px;">
      <div class="panel-heading text-center" style=" border-color: #59A9C2;  border-style: solid;  border-width: 0px 0px 5px 0px; background-color: #F8F8F8; color:#00587A;"><h3>Administradores de cinépolis</h3></div>

        <br>
        <div class="panel-body">

          <table class="table">
          <thead>
           <tr  style="border-color: #00587A;  border-style: solid;  border-width: 0px 0px 2px 0px; color:#00587A;">
             <th>id</th>
             <th>Nombre de usuario</th>
             <th>Contraseña</th>
             <th>Acciones</th>
           </tr>
          </thead>
          <tbody>

          <?php foreach($this->admins as $admin): ?>
           <tr style=" border-color: #59A9C2;  border-style: solid;  border-width: 0px 0px 0px 0px; color:#00587A;">
             <td><?php echo $admin["id"];?></td>
             <td><?php echo $admin["username"];?></td>
             <td><?php echo $admin["password"];?></td>
             <td>  <a  style="margin-left:5%; color: #00587A;"  class="btn btn-default " href="<?php echo URL;?>Admin/editar/<?php echo $admin["id"];?>"><span class="glyphicon ">   Editar </span></a>

               <button style="margin-left:5%; background-color: #00587A;" class="btn btn-primary" data-toggle="modal" onclick="eliminar(<?php echo $admin["id"];?>)"><span class="glyphicon">   Eliminar</span></button>
             </td>

           </tr>

         <?php endforeach ;?>

          </tbody>
          </table>

        </div>

        <br>

	</div>

		</div>


</body>
</hmtl>
