<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Editar Ciudad</h4>

              </div>


                  	<div class="panel-body">



          <form  action="<?php echo URL."Ciudad/update"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
            <input type="hidden"  name="id" value="<?php echo $this->ciudad[0]["id"]; ?>">
              <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="<?php echo $this->ciudad[0]["nombre"]; ?>">
            </div>

                  <div class="form-group">
                    <label for="">Pais</label>
                       <select name="pais_id" class ="form-control">
                           <?php foreach ( $this->paises as $pais ): ?>

                            <option value="<?php echo $pais["id"]; ?>"

                            <?php if ($this->ciudad[0]["Pais_id"] == $pais["id"]) { echo "selected='selected'"; }?>

                              ><?php echo $pais["nombre"]; ?></option>
                           <?php endforeach; ?>
                        </select>


                        </div>

                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Ciudad" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
