<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Editar Cliente</h4>

              </div>


                  	<div class="panel-body">



          <form  action="<?php echo URL."Cliente/update"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
            <input type="hidden"  name="id" value="<?php echo $this->cliente[0]["id"]; ?>">
            </div>
            <div class="form-group ">
            <label for="">Nombre</label>
            <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="<?php echo $this->cliente[0]["nombre"]; ?>">
            </div>
            <div class="form-group ">
            <label for="">Contraseña</label>
            <input type="text" class="form-control" name="contrasena" placeholder="contraseña" value="<?php echo $this->cliente[0]["contrasena"]; ?>">
            </div>
            <div class="form-group ">
            <label for="">Numero tarjeta Ciné polis</label>
            <input type="text" class="form-control" name="identificacion" placeholder="Identificacion" value="<?php echo $this->cliente[0]["identificacion"]; ?>">
            </div>
            <div class="form-group ">
            <label for="">Email</label>
            <input type="text" class="form-control" name="email" placeholder="pepito@pepito.com" value="<?php echo $this->cliente[0]["email"]; ?>">
            </div>
            <div class="form-group ">
            <label for="">Fecha de Nacimiento</label>
            <input type="date" class="form-control" value="<?php echo $this->cliente[0]["fechaNacimiento"]; ?>" name="fechaNacimiento">
            </div>
            <label for="">Teléfono</label>
            <input type="text" class="form-control" name="telefono" placeholder="30167543891" value="<?php echo $this->cliente[0]["telefono"]; ?>">
            </div>
            <label for=""></label>
            <input type="hidden" class="form-control">
            </div>

            <div class="col-lg-4">     </div>
                    <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                        <div class="col-lg-4">     </div>

              </form>
        </div>
        </div>
        <a type="button" href="<?php echo URL ?>Cliente" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

        </a>
    </div>
</div>



</div>



</body>
</hmtl>
