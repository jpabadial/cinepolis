<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Editar Funcion</h4>

              </div>



                  	<div class="panel-body">



          <form  action="<?php echo URL."Funcion/update"  ?>" enctype="multipart/form-data"  method="post">
                    <?php foreach ( $this->funciones as $funcion ): ?>
            <div class="form-group ">
            <input type="hidden"  name="id" value="<?php echo $funcion["id"]; ?>">
                </div>
            <div class="form-group">
              <label for="">Sala</label>
                 <select name="sala" class ="form-control">
                     <?php foreach ( $this->salas as $sala ): ?>

                      <option value="<?php echo $sala["id"]; ?>"

                      <?php if ($funcion["Sala_id"] === $sala["id"]) { echo "selected='selected'"; }?>

                        ><?php echo "Sala N° ".$sala["numero"]; ?></option>
                     <?php endforeach; ?>
                  </select>


                  </div>

                  <div class="form-group">
                    <label for="">Pelicula</label>
                       <select class ="form-control" name="pelicula">
                           <?php foreach ( $this->peliculas as $pelicula ): ?>
                            <option value="<?php echo $pelicula["id"]; ?>"
                                <?php if ($funcion["Pelicula_id"] === $pelicula["id"]) { echo "selected='selected'"; }?>

                              ><?php echo $pelicula["titulo"]; ?></option>
                           <?php endforeach; ?>
                        </select>


                        </div>



              <div class="form-group">
<label for="">Fecha</label>
                  <input type="date" class="form-control" value="<?php echo $funcion["fecha"]; ?>" name="fecha">
            </div>

            <div class="form-group">
              <label for="">Hora</label>
                    <div class='input-group' id='datetimepicker3'>
                        <input  placeholder="hh:mm" data-format="hh:mm" min="5" value="<?php echo $funcion["hora"]; ?>"  name="hora" class="form-control" />
                        <span class="input-group-addon add-on">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                <script type="text/javascript">
                $(function() {
                   $('#datetimepicker3').datetimepicker({

                    pickDate: false
                   });
                 });
                       </script>


                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                          <?php endforeach; ?>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Funcion" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
