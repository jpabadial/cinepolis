<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Editar Pelicula</h4>

              </div>


                  	<div class="panel-body">



          <form  action="<?php echo URL."Pelicula/update"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
 <?php foreach ( $this->peliculas as $pelicula ): ?>
            <input type="hidden"  name="id" value="<?php echo $pelicula["id"]; ?>">
              <input type="text" class="form-control" name="titulo" placeholder="Titulo" value="<?php echo $pelicula["titulo"]; ?>">
            </div>

            <div class="form-group ">
              <label for="">Subtitulada</label>
                <select name="subtitulada">
                    <option value="1">Si</option>
                    <option value="2">No</option>
                </select>
            </div>
          <div class="form-group">
            <label for="">Poster</label>

              <input type="file" class="form-control" name="poster" >

            </div>


            <div class="form-group">
                  <label for="">Fecha de Estreno</label>
                  <input type="date" class="form-control" name="fecha_estreno" value="<?php echo $pelicula["fecha_estreno"]; ?>">
            </div>

          <div class="form-group">
            <label for="">Categoria</label>
               <select name="categoria">
                   <?php foreach ( $this->categorias as $categoria ): ?>
                     <option value="<?php echo $categoria["id"];?>"

                         <?php if ($pelicula["Categoria_id"] === $categoria["id"]) { echo "selected='selected'"; }?>
                        ><?php echo $categoria["nombre"];?>
                   </option>
                   <?php endforeach;?>
                </select>


                </div>


<div class="form-group">
  <label for="">Sinopsis</label>
  <textarea name="sinopsis" class="form-control" rows="8" ><?php echo $pelicula["Sinopsis"]; ?></textarea>
</div>

                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                    <?php endforeach; ?>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Pelicula" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
