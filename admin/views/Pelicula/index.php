<?php include MODULE."head.php"; ?>

<body style="background-color: #00587A;">
    <?php include MODULE."headerC.php"; ?>

  <div class="container">


<div class="row">

    <div class="col-lg-12">
            	<div class="panel-heading">
                <br>
    		  		<div class="pull-left">
    		        	<a class="btn btn-default"  style="width:200%; color: #00587A; font-weight:bolder;"href="<?php echo URL;?>Pelicula/crear">Agregar Pelicula <span class=""></span></a>
                </div>
    		  	</div>
            <br><br>
    </div>

    <div class="col-lg-12">
      <?php foreach($this->peliculas as $pelicula): ?>

       				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="panel panel-primary">
                  <div class="panel-heading text-left" style="border-color: #59A9C2;  border-style: solid;  border-width: 0px 0px 5px 0px; background-color: #F8F8F8; color:#00587A;">
                <h4><?php echo $pelicula["titulo"];?></h4>
                </div>
                  	<div class="panel-body">

		                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="movie" style=" background: url(<?php echo URLIMAGES;?><?php echo $pelicula["poster"];?>);background-size:cover;">
                      </div>
		                </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <ul class="informacionP" style="padding:0px !important; margin-top:10px;" >
                      <li class="list-group-item"> <h5 style="color: #00587A; font-weight:bolder;"> Subtitulada: </h5> <span class="subtitulada"><?php
                      $respuesta1=1;
                    $respuesta2=2; $subtitulada=$pelicula["subtitulada"];
                    if($subtitulada==$respuesta1){
                    echo "Sí";
                    }
                    if($subtitulada==$respuesta2){
                    echo "No";
                    }
                      ?></span></li>
                        <li class="list-group-item"> <h5 style="color: #00587A; font-weight:bolder;">Fecha de estreno: </h5><span><B><?php echo $pelicula["fecha_estreno"];?></B></span></li>

                          <li class="list-group-item"> <h5 style="color: #00587A; font-weight:bolder;">Categoria:  </h5> <span><b>
                              <?php foreach($this->categorias as $categoria): ?>
                            <?php if ($pelicula["Categoria_id"]===$categoria["id"]) {
                                    echo  $categoria["nombre"]  ;
                                  }
                          ?>
                        <?php endforeach; ?>
                              </b></span></li>
                    </ul>

                   </div>

		                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left separador_info">
		                    <div class="btn-group contenedor_botones" id="app" role="group" aria-label="...">
                            <br>
                            <br>
		                        <a href="<?php echo URL;?>Pelicula/editar/<?php echo $pelicula["id"];?>"  style="color: #00587A;"class="btn btn-default"><span class=""></span> Editar</a>
 								            <button class="button_eliminar_campo btn btn-primary" style="background-color: #00587A; " data-toggle="modal" onclick="eliminar(<?php echo $pelicula["id"];?>)"><span class=""></span> Eliminar</button>
			                </div>
	                    </div>
                    </div>
                  </div>
	            	</div>


  <?php endforeach; ?>

</div>
</div>
  </div>
<br>
</body>
</hmtl>
