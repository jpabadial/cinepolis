<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Agregar Reserva</h4>

              </div>
                  	<div class="panel-body">

          <form  action="<?php echo URL."Reserva/save"  ?>" enctype="multipart/form-data"  method="post">

            <div class="form-group">
              <label for="">Forma de pago</label>
                 <select name="forma_de_pago" class ="form-control">
                     <option name="forma_de_pago" value="Efectivo en taquilla">Efectivo en taquilla</option>
                      <option name="forma_de_pago" value="Debito">Débito</option>
                      <option name="forma_de_pago" value="Credito">Crédito</option>
                  </select>
                  </div>

                  <div class="form-group">
                    <label for="">Estado del pago</label>
                       <select name="estado_pago" class ="form-control">
                           <option name="estado_pago" value="Realizado">Realizado</option>
                            <option name="estado_pago" value="Pendiente">Pendiente</option>
                        </select>

                        </div>

                  <div class="form-group">
                    <label for="">¿Cuántas sillas desea reservar?</label>
                       <select name="numSillas" class ="form-control">
                           <option name="numSillas" value="1">1</option>
                            <option name="numSillas" value="2">2</option>
                            <option name="numSillas" value="3">3</option>
                            <option name="numSillas" value="4">4</option>
                            <option name="numSillas" value="5">5</option>
                        </select>
                  </div>


            <div class="form-group">
              <label for="">Cliente</label>
                 <select name="cliente_id" class ="form-control">
                     <?php foreach ( $this->clientes as $cliente ): ?>
                      <option value="<?php echo $cliente["id"]; ?>"><?php echo $cliente["id"].": ".$cliente["nombre"]; ?></option>
                     <?php endforeach; ?>
                  </select>


                  </div>

                  <div class="form-group">
                    <label for="">Información de la función</label>
                       <select class ="form-control" name="funcion_id">
                           <?php foreach ( $this->funciones as $funcion ): ?>
                            <option value="<?php echo $funcion["id"]; ?>"><?php

                            foreach ($this->peliculas as $pelicula) {

                              if($funcion["Pelicula_id"]==$pelicula["id"]){

                                    echo "id: ".$funcion["id"]."  /  "."Nombre de la película:   ".$pelicula["titulo"];
                              }

                              }

                            ?></option>
                           <?php endforeach; ?>
                        </select>
                        </div>


                <script type="text/javascript">
                $(function() {
                   $('#datetimepicker3').datetimepicker({

                    pickDate: false
                   });
                 });
                       </script>


                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Reserva" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
