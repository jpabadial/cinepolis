<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Editar Reserva</h4>

              </div>



                  	<div class="panel-body">



          <form  action="<?php echo URL."Reserva/update"  ?>" enctype="multipart/form-data"  method="post">

            <div class="form-group ">
            <input type="hidden"  name="id" value="<?php echo $this->reserva[0]["id"]; ?>">
            </div>

            <div class="form-group ">
            <label for="">El ID de la reserva que va a editar es: </label>
            <label for=""><?php echo $this->reserva[0]["id"]; ?> </label>
            </div>

            <div class="form-group">
              <label for="">Forma de pago </label>
                    <select name="forma_de_pago" class ="form-control">
                        <?php
                        $array = array("Efectivo en Taquilla","Debito", "Credito");
                        foreach ( $array as $valor ): ?>

                         <option value="<?php print_r($valor); ?>"

                         <?php if ($this->reserva[0]["forma_de_pago"] == $valor) { echo "selected='selected'"; }?>

                           ><?php print_r($valor); ?></option>
                        <?php endforeach; ?>
                     </select>

                  </div>

                  <div class="form-group">
                    <label for="">Estado del pago </label>
                          <select name="estado_pago" class ="form-control">
                              <?php
                              $array = array("Pendiente","Realizado");
                              foreach ( $array as $valor ): ?>

                               <option value="<?php print_r($valor); ?>"

                               <?php if ($this->reserva[0]["estado_pago"] == $valor) { echo "selected='selected'"; }?>

                                 ><?php print_r($valor); ?></option>
                              <?php endforeach; ?>
                           </select>

                        </div>

                        <div class="form-group">
                          <label for="">Numero de sillas </label>
                                <select name="numSillas" class ="form-control">
                                    <?php
                                    $array = array("1","2","3","4","5");
                                    foreach ( $array as $valor ): ?>

                                     <option value="<?php print_r($valor); ?>"

                                     <?php if ($this->reserva[0]["precio"]/8000 == $valor) { echo "selected='selected'"; }?>

                                       ><?php print_r($valor); ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>

                              <div class="form-group">
                                <label for="">Información del cliente </label>
                                <select name="cliente_id" class ="form-control">
                                    <?php foreach ( $this->clientes as $cliente ): ?>
                                     <option value="<?php echo $cliente["id"]; ?>"

                                        <?php if ($this->reserva[0]["Cliente_id"] == $cliente["id"]) { echo "selected='selected'"; }?>

                                       ><?php echo $cliente["id"].": ".$cliente["nombre"]; ?></option>
                                    <?php endforeach; ?>
                                 </select>
                                    </div>


                                    <div class="form-group">
                                      <label for="">Información de la funcion </label>
                                      <select name="funcion_id" class ="form-control">
                                          <?php foreach ( $this->funciones as $funcion ): ?>
                                           <option value="<?php echo $funcion["id"]; ?>"

                                              <?php if ($this->reserva[0]["Funcion_id"] == $funcion["id"]) { echo "selected='selected'"; }?>

                                             >
                                             <?php
                                             foreach ($this->peliculas as $pelicula) {
                                               if($funcion["Pelicula_id"]==$pelicula["id"]){
                                                     echo "id: ".$funcion["id"]."  /  "."Nombre de la película:   ".$pelicula["titulo"];
                                               }

                                               }
                                              ?></option>
                                          <?php endforeach; ?>
                                       </select>
                                          </div>


                <script type="text/javascript">
                $(function() {
                   $('#datetimepicker3').datetimepicker({

                    pickDate: false
                   });
                 });
                       </script>


                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>

                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Funcion" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
