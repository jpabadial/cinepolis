<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Agregar Sala</h4>

              </div>

                  	<div class="panel-body">

          <form  action="<?php echo URL."Sala/save"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
                <label>Numero</label>
              <input type="text" class="form-control" name="numero" placeholder="numero de la sala">
            </div>
            <div class="form-group ">
                <label>Capacidad</label>
              <input type="text" class="form-control" name="capacidad" placeholder="Capacidad de la sala">
            </div>

            <div class="form-group">
              <label for="">Teatro</label>
                 <select name="teatro_id" class ="form-control">

                   <?php foreach($this->teatros as $teatro): ?>

                     <option name="teatro_id" value="<?php echo $teatro["id"]; ?>">
                       <?php echo $teatro["nombre"]; ?></option>

                   <?php endforeach; ?>


                  </select>

                  </div>

                  <div class="form-group">
                    <label for="">Tipo de la sala</label>
                       <select name="tipo_id" class ="form-control">

                         <?php foreach($this->tiposSala as $tipoSala): ?>

                           <option name="tipo_id" value="<?php echo $tipoSala["id"]; ?>">
                             <?php echo $tipoSala["nombre"]; ?></option>

                         <?php endforeach; ?>


                        </select>

                        </div>

                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Sala" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
