<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Editar Sala</h4>

              </div>


                  	<div class="panel-body">



          <form  action="<?php echo URL."Sala/update"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
            <input type="hidden"  name="id" value="<?php echo $this->sala[0]["id"]; ?>">
              <input type="text" class="form-control" name="numero" placeholder="Numero de la sala" value="<?php echo $this->sala[0]["numero"]; ?>">
            </div>
            <div class="form-group ">
            <input type="hidden"  name="capacidad" value="<?php echo $this->sala[0]["capacidad"]; ?>">
              <input type="text" class="form-control" name="capacidad" placeholder="Capacidad de la sala" value="<?php echo $this->sala[0]["capacidad"]; ?>">
            </div>

                  <div class="form-group">
                    <label for="">Teatro</label>
                       <select name="teatro_id" class ="form-control">
                           <?php foreach ( $this->teatros as $teatro ): ?>

                            <option value="<?php echo $teatro["id"]; ?>"

                            <?php if ($this->sala[0]["Teatro_id"] == $teatro["id"]) { echo "selected='selected'"; }?>

                              ><?php echo $teatro["nombre"]; ?></option>
                           <?php endforeach; ?>
                        </select>


                        </div>

                        <div class="form-group">
                          <label for="">Tipo de la sala</label>
                             <select name="tipo_id" class ="form-control">
                                 <?php foreach ( $this->tiposSala as $tipoSala ): ?>

                                  <option value="<?php echo $tipoSala["id"]; ?>"

                                  <?php if ($this->sala[0]["Tipo_id"] == $tipoSala["id"]) { echo "selected='selected'"; }?>

                                    ><?php echo $tipoSala["nombre"]; ?></option>
                                 <?php endforeach; ?>
                              </select>


                              </div>

                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Sala" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
