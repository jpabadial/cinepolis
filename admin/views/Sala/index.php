<?php include MODULE."head.php"; ?>

<body style="background-color: #00587A;">
    <?php include MODULE."headerC.php"; ?>

    <div class="row">
				<div class="margen_top_login"></div>
	<div class="col-xs-0 col-sm-0 col-md-1 col-lg-1"></div>
	<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="panel-heading content-center">
      <div class="pull-left" style="margin-bottom:10px; ">
      <a class="btn btn-default" style="width:200%; color: #00587A; font-weight:bolder;" href="<?php echo URL;?>Sala/crear">Agregar Sala <span class=""></span></a>
      </div>
    </div>
    <br>
    <br>
		<div class="panel panel-default" style="  border-color: #59A9C2;  border-style: solid;  border-width: 0px;">
      <div class="panel-heading text-center" style=" border-color: #59A9C2;  border-style: solid;  border-width: 0px 0px 5px 0px; background-color: #F8F8F8; color:#00587A;"><h3>Salas</h3></div>

        <br>
        <div class="panel-body">

          <table class="table">
          <thead>
           <tr  style="border-color: #00587A;  border-style: solid;  border-width: 0px 0px 2px 0px; color:#00587A;">
             <th>id</th>
             <th>Numero</th>
             <th>Capacidad</th>
             <th>Teatro</th>
             <th>Tipo sala</th>
           </tr>
          </thead>
          <tbody>

          <?php foreach($this->salas as $sala): ?>
           <tr style=" border-color: #59A9C2;  border-style: solid;  border-width: 0px 0px 0px 0px; color:#00587A;">
             <td><?php echo $sala["id"];?></td>
             <td><?php echo $sala["numero"];?></td>
             <td><?php echo $sala["capacidad"];?></td>
             <td>
             <?php foreach($this->teatros as $teatro): ?>

              <?php

               if ($teatro["id"]===$sala["Teatro_id"]) {
                      echo  $teatro["nombre"]  ;
                } ?>

             <?php endforeach; ?>
             </td>

             <td>
             <?php foreach($this->tiposSala as $tipoSala): ?>

              <?php

               if ($tipoSala["id"]===$sala["Tipo_id"]) {
                      echo  $tipoSala["nombre"]  ;
                } ?>

             <?php endforeach; ?>
             </td>

             <td>  <a  style="margin-left:5%; color: #00587A;"  class="btn btn-default " href="<?php echo URL;?>Sala/editar/<?php echo $sala["id"];?>"><span class="glyphicon ">   Editar </span></a>

               <button style="margin-left:5%; background-color: #00587A;" class="btn btn-primary" data-toggle="modal" onclick="eliminar(<?php echo $sala["id"];?>)"><span class="glyphicon">   Eliminar</span></button>
             </td>

           </tr>

         <?php endforeach ;?>

          </tbody>
          </table>

        </div>

        <br>

	</div>

		</div>


</body>
</hmtl>
