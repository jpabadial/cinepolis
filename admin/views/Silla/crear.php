<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Agregar Silla</h4>

              </div>

                  	<div class="panel-body">

          <form  action="<?php echo URL."Silla/save"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
                <label>Numero</label>
              <input type="text" class="form-control" name="numero" placeholder="numero de la Silla">
            </div>
            <div class="form-group ">
                <label>Fila</label>
              <input type="text" class="form-control" name="fila" placeholder="Fila en la que está ubicada la silla">
            </div>

            <div class="form-group">
              <label for="">Estado       </label>
                    (  1: Disponible 2: No disponible )
                 <select name="estado" class ="form-control">
                     <option name="estado" value="1">1</option>
                      <option name="estado" value="2">2</option>
                  </select>

                  </div>

            <div class="form-group">
              <label for="">Sala</label>
                 <select name="sala_id" class ="form-control">

                   <?php foreach($this->salas as $sala): ?>

                     <option name="sala_id" value="<?php echo $sala["id"]; ?>">
                       <?php echo $sala["numero"]; ?></option>

                   <?php endforeach; ?>


                  </select>

                  </div>

                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Sala" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
