<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Editar Silla</h4>

              </div>


                  	<div class="panel-body">



          <form  action="<?php echo URL."Silla/update"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
            <input type="hidden"  name="id" value="<?php echo $this->silla[0]["id"]; ?>">
              <input type="text" class="form-control" name="numero" placeholder="Numero de la silla" value="<?php echo $this->silla[0]["numero"]; ?>">
            </div>
            <div class="form-group ">
            <input type="hidden"  name="fila" value="<?php echo $this->silla[0]["fila"]; ?>">
              <input type="text" class="form-control" name="fila" placeholder="Filla en donde está ubicada la silla" value="<?php echo $this->silla[0]["fila"]; ?>">
            </div>

            <div class="form-group">
              <label for="">Estado </label>
                    (  1: Disponible 2: No disponible )
                    <select name="estado" class ="form-control">
                        <?php
                        $array = array(1,2);
                        foreach ( $array as $valor ): ?>

                         <option value="<?php print_r($valor); ?>"

                         <?php if ($this->silla[0]["estado"] == $valor) { echo "selected='selected'"; }?>

                           ><?php print_r($valor); ?></option>
                        <?php endforeach; ?>
                     </select>

                  </div>

                        <div class="form-group">
                          <label for="">Sala</label>
                             <select name="sala_id" class ="form-control">
                                 <?php foreach ( $this->salas as $sala ): ?>

                                  <option value="<?php echo $sala["id"]; ?>"

                                  <?php if ($this->silla[0]["Sala_id"] == $sala["id"]) { echo "selected='selected'"; }?>

                                    ><?php echo $sala["numero"]; ?></option>
                                 <?php endforeach; ?>
                              </select>

                              </div>

                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Silla" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
