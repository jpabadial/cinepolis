<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Agregar Teatro</h4>

              </div>

                  	<div class="panel-body">

          <form  action="<?php echo URL."Teatro/save"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
                <label>Nombre</label>
              <input type="text" class="form-control" name="nombre" placeholder="nombre del teatro">
            </div>
            <div class="form-group ">
                <label>Teléfono</label>
              <input type="text" class="form-control" name="telefono" placeholder="teléfono del teatro">
            </div>
            <div class="form-group ">
              <label>Dirección</label>
              <input type="text" class="form-control" name="direccion" placeholder="dirección del teatro">
            </div>

            <div class="form-group">
              <label for="">Ciudad</label>
                 <select name="ciudad_id" class ="form-control">

                   <?php foreach($this->ciudades as $ciudad): ?>

                     <option name="ciudad_id" value="<?php echo $ciudad["id"]; ?>">
                       <?php echo $ciudad["nombre"]; ?></option>

                   <?php endforeach; ?>


                  </select>

                  </div>

                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Teatro" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
