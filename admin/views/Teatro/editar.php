<?php include MODULE."head.php"; ?>

<body style="background-color: #dadada;">

    <?php include MODULE."headerC.php"; ?>


      <div class="container">

        <div class="row">
          <div class="col-xs-0 col-sm-0 col-md-3 col-lg-3"></div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          		<div class="panel panel-primary">

              <div class="panel-heading">
                <h4>Editar Teatro</h4>

              </div>


                  	<div class="panel-body">



          <form  action="<?php echo URL."Teatro/update"  ?>" enctype="multipart/form-data"  method="post">
            <div class="form-group ">
            <input type="hidden"  name="id" value="<?php echo $this->teatro[0]["id"]; ?>">
              <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="<?php echo $this->teatro[0]["nombre"]; ?>">
            </div>
            <div class="form-group ">
            <input type="hidden"  name="telefono" value="<?php echo $this->teatro[0]["telefono"]; ?>">
              <input type="text" class="form-control" name="telefono" placeholder="Telefono" value="<?php echo $this->teatro[0]["telefono"]; ?>">
            </div>
            <div class="form-group ">
            <input type="hidden"  name="direccion" value="<?php echo $this->teatro[0]["direccion"]; ?>">
              <input type="text" class="form-control" name="direccion" placeholder="direccion" value="<?php echo $this->teatro[0]["direccion"]; ?>">
            </div>

                  <div class="form-group">
                    <label for="">Ciudad</label>
                       <select name="ciudad_id" class ="form-control">
                           <?php foreach ( $this->ciudades as $ciudad ): ?>

                            <option value="<?php echo $ciudad["id"]; ?>"

                            <?php if ($this->teatro[0]["Ciudad_id"] == $ciudad["id"]) { echo "selected='selected'"; }?>

                              ><?php echo $ciudad["nombre"]; ?></option>
                           <?php endforeach; ?>
                        </select>


                        </div>

                <div class="col-lg-4">     </div>
                        <div class="col-lg-4">      <button type="submit" class="btn btn-primary  " style="width: 100%;">Guardar</button> </div>
                            <div class="col-lg-4">     </div>

                    </form>
                </div>

                </div>
                <a type="button" href="<?php echo URL ?>Teatro" class="btn btn-success"><span class="glyphicon glyphicon-menu-left"></span>Volver

                </a>
            </div>
      </div>



        </div>



</body>
</hmtl>
