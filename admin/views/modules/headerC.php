<?php if(isset($this->admin)): ?>

  <header style="	display: flex;
  	justify-content: center;
  	align-items: center;
  	align-content: center;
  	width: 100%;
  	max-height: 50%;
  	background-color: #0b5ba1; padding: oem 1em 0em 1em;">



    <nav style="	display: flex;
    	flex-flow:  row nowrap;
    	justify-content: space-around;
    	align-items: flex-start;
    	width: 60%;
    	align-self: flex-start;">

      <div id="logoCinepolis" class="" style="width:8%; margin-right: 18%;">
        <img src="<?php print(URL); ?>public/assets/images/cinepolisLogo.png" alt="" style="width:300%;">
      </div>

      <h1 style="color: #F8F8F8; align-self:center;">Panel Administrativo de Ciné polis</h1>

  </nav>
  </header>


<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar">
      <div class="navbar-header btn-left-header">
        <br>
				<a class="navbar-brand" href="<?php echo URL?>" style="color: #005893; font-size:250%;"></a>
			</div>
      <br>
      <ul class="nav navbar-nav navbar-left" style="padding: 0.1em 0em 0em 0em;">
          <li  style=" margin-right: 0em;"><a href="#"></a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Index/index">Hoy</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Pelicula/index">Peliculas</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Funcion/index">Funciones</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>CategoriaPelicula/index">Categorías</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Ciudad/index">Ciudades</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Pais/index">Paises</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Teatro/index">Teatros</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Sala/index">Salas</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Silla/index">Sillas</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>TipoSala/index">Tipos Salas</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Reserva/index">Reservas</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Cliente/index">Clientes</a></li>
          <li class="panel panel-primary" style=" margin-right: 1em;"><a href="<?php echo URL;?>Admin/index">Administradores</a></li>

          <li class="dropdown panel panel-info" style=" margin-left: 1em;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><span class="glyphicon"> </span><?php echo " ".$this->admin->getUsername(); ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#"><span class="glyphicon "> </span> Perfil </a></li>
            <li class="divider"></li>
            <li><a href="<?php echo URL; ?>Usuario/logout"><span class="glyphicon"></span> Cerrar Sesió n </a></li>
          </ul>
          </li>

        <li><a href="#"></a></li>

      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"></a></li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<script>
    $("#logoutBtn").click(function(){
       document.location = "<?php echo URL; ?>Usuario/logout";
    });
</script>
<?php endif; ?>
