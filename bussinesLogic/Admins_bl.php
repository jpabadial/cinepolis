<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admins_bl
 *
 * @author pabhoz
 */
class Admins_bl {

    public function login($username,$password){
        $adm = Admin::getBy("username", $username);
        if($adm == null){ return false; }
        return ($adm->getPassword() == $password)? true : false;
    }

    public function listarAdmins() {
        $admins = Admin::getAll();
        return $admins;
    }

    public function save($adminArr){
          $adminArr["id"] = null;
           $fun = Admin::instanciate($adminArr);

           $r = $fun->create();

          print_r($r);

           return $r;
       }


       public function update($adminArr){

          $fun = Admin::instanciate($adminArr);
          $fun->setId($adminArr["id"]);
          $fun->getUsername($adminArr["username"]);
          $fun->setPassword($adminArr["password"]);
          $r = $fun->update("id",$fun->getId());
          return $r;

       }

       public function delete($id){
         $funDelete=Admin::where("id",$id);
         $fun = Admin::instanciate($funDelete[0]);
         $r=Admin::mydelete($fun->getId());

       }

}
