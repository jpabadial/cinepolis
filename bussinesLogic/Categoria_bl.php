<?php

class Categoria_bl {

    public function listarCategorias() {
        $categoria = CategoriaPelicula::getAll();
        return $categoria;
    }

    public function guardarCategoria(){
        if(isset($_GET["nombre"]) ){
             $nombre=$_GET["nombre"];
             $categoria= new CategoriaPelicula($nombre);
             $categoria->create();
             print_r($categoria);
        }
    }

    public function getCategoria($id) {
      $categoria=CategoriaPelicula::getBy("id", $id);
      return($categoria);
    }

    public function update($categoriaArr){

       $fun = CategoriaPelicula::instanciate($categoriaArr);
       $fun->setId($categoriaArr["id"]);
       $fun->setNombre($categoriaArr["nombre"]);
       $r = $fun->update("id",$fun->getId());
       return $r;

    }

    public function delete($id){
      $funDelete=CategoriaPelicula::where("id",$id);
      $fun = CategoriaPelicula::instanciate($funDelete[0]);
      $r=CategoriaPelicula::mydelete($fun->getId());

    }

    public function save($categoriaArr){
          $categoriaArr["id"] = null;
           $fun = CategoriaPelicula::instanciate($categoriaArr);

           //$fun->setNombre($sala->getId());

           $r = $fun->create();

          print_r($r);

           return $r;
       }


}
