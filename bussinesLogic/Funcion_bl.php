<?php

class Funcion_bl {

    public function listarFunciones() {
        $values = Funcion::getAll();

        if(!empty($values)){
            return $values;
        }else{
            return "No hay Funciones";
        }
    }

  public function getFuncionAJAX($id){
    //echo $id;
    $funcion=Funcion::getBy("id", $id,true);
    print(json_encode($funcion));
}

public function getFunciones($id_pelicula){
  //echo $id;
  $funciones=Funcion::getBy("pelicula_id", $id_pelicula,true);

  foreach ($funciones as $key => $funcion) {
    $funciones[$key]["sala"] = Sala::getById($funcion["Sala_id"])->toArray();
  }
  return $funciones;
}

public function save($funcionArr){

    $this->validateKeys(["sala","pelicula","fecha","hora"], $_POST);

      $funcionArr["id"] = null;
      $funcionArr["Sala_id"] = null;
      $funcionArr["Pelicula_id"] = null;


       $sala = Sala::getById($funcionArr["sala"]);
       unset($funcionArr["sala"]);
       $pelicula = Pelicula::getById($funcionArr["pelicula"]);
       unset($funcionArr["pelicula"]);

       $fun = Funcion::instanciate($funcionArr);
       $fun->setSala_id($sala->getId());
       $fun->has_one("Pelicula",$pelicula);
      $fun->has_one("Sala",$sala);

      $r = $fun->create();

      print_r($r);

       return $r;
   }


   public function update($funcionArr){

      $funcionArr["Sala_id"] = null;
      $funcionArr["Pelicula_id"] = null;
       $sala = Sala::getById($funcionArr["sala"]);
       unset($funcionArr["sala"]);
       $pelicula = Pelicula::getById($funcionArr["pelicula"]);
       unset($funcionArr["pelicula"]);


         $fun = Funcion::instanciate($funcionArr);


       $fun->setId($funcionArr["id"]);
       $fun->setHora($funcionArr["hora"]);
       $fun->setFecha($funcionArr["fecha"]);
       $fun->setPelicula_id($pelicula->getId());
       $fun->setSala_id($sala->getId());
               $r = $fun->update(id,$fun->getId());
   return $r;



   }

   public function delete($id){

     $funDelete=Funcion::where("id",$id);
     $fun = Funcion::instanciate($funDelete[0]);
       //$r=Funcion::deleteRelationships2();
     $r=Funcion::mydelete($fun->getId());

   }



  }
