<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Peliculas_bl {

  public function Peliculas(){
         //$query="SELECT pe.id,pe.titulo,pe.subtitulada,pe.poster,pe.fecha_estreno,cp.nombre FROM pelicula pe ,categoriapelicula cp where pe.categoria_id=cp.id";
           $movies = Pelicula::getAll();

       return $movies;
   }
   public function update($peliculaArr, $poster=null){

   $peliculaArr["Categoria_id"] = null;
   $peliculaArr["poster"] = null;
   $peliculaArr["Sinopsis"] = null;

  $cat = CategoriaPelicula::getById($peliculaArr["categoria"]);
   unset($peliculaArr["categoria"]);

   $peliold=Pelicula::where("id",$peliculaArr["id"]);
   $peli = Pelicula::instanciate($peliculaArr);

    if ($poster!=NULL) {
      $i = Image::upload("../cinepolis/public/assets/images/", "poster", $peli->getId());
      $i = substr($i, 13);
      $peli->setPoster($i);

    }else{
   $peli->setPoster($peliold[0]["poster"]);


    }

    $peli->setId($peliculaArr["id"]);
    $peli->setTitulo($peliculaArr["titulo"]);
    $peli->setSinopsis($peliculaArr["sinopsis"]);
    $peli->setSubtitulada($peliculaArr["subtitulada"]);
    $peli->setFecha_estreno($peliculaArr["fecha_estreno"]);
    $peli->setCategoria_id($cat->getId());

    $r = $peli->update("id",$peli->getId());

    return $r;



}

public function delete($id){
  $peli=Pelicula::getBy("id",$id);
  $r=Pelicula::mydelete($peli->getId());
  return $r;

}


  public function save($peliculaArr, $poster){
    $peliculaArr["id"] = null;
    $peliculaArr["poster"] = null;
    $peliculaArr["Categoria_id"] = null;
    $peliculaArr["Sinopsis"] = null;

    $cat = CategoriaPelicula::getById($peliculaArr["categoria"]);
    unset($peliculaArr["categoria"]);

    $peli = Pelicula::instanciate($peliculaArr);

    $peli->setSinopsis($peliculaArr["sinopsis"]);
    $peli->setSinopsis($peliculaArr["titulo"]);
    $peli->setSinopsis($peliculaArr["subtitulada"]);
    $peli->setSinopsis($peliculaArr["fecha_estreno"]);

    $peli->has_one("Categoria",$cat);

    $r = $peli->create();


    $i = Image::upload("../cinepolis/public/assets/images/", "poster", $r["getID"]);
    $i = substr($i, 13);
    $peli->setPoster($i);
    $r = $peli->update();

    //return $r;
  }




  public function listarPeliculas(){



      $date= 'now()';


          $movies = Pelicula::whereMenor('fecha_estreno',$date);


      return $movies;
  }

  public function proximosEstrenos(){

  //  setlocale(LC_ALL,"es_ES");


    $date= 'now()';


      $fecha = Pelicula::whereMayor('fecha_estreno',$date);
      //print_r(Pelicula::getFecha_estreno());
      return $fecha;

  }



  public function borrame(){
      $func = new Funcion(null, "16:30", "2017-04-14", "1", "12");
      $func->create();

      $res = new Reserva(null, "efectivo", "pendiente", 24000, 1, 1);
      $res->create();

      $silla = new Silla(null, 3, 2, 1, 1);
      $silla->create();

      $res->has_many("Sillas", $silla);
      $res->update();
  }


  public function buscarPeliculaPorTitulo(){
     if(isset($_GET["titulo"])){
         $titulo=$_GET["titulo"];
         $pelicula = Pelicula::getBy("titulo", $titulo);
         if(!is_null($pelicula)){
             print_r($pelicula);
         }else{
             echo "La pelicula no existe";
         }
     }
 }

 public function buscarPeliculaPorid($id){
     //echo $id;
     $pelicula=Pelicula::getBy("id", $id);
     return($pelicula);

}

}
