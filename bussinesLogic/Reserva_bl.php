<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Reserva_bl {

  public function listarReservas(){
      $values= Reserva::getAll();
      if(!empty($values)){
            return $values;
        }else{
            return "No hay Reservas";
        }
  }

public function reservascliente($idC){

    $values= Reserva::where("Cliente_id",$idC);

    return $values;


}

    public function saveReservaAdmin($reservaArr){

      $reservaArr["id"]= null;
      $reservaArr["Cliente_id"]= null;
      $reservaArr["Funcion_id"]= null;

      $cliente=Cliente::getById($reservaArr["cliente_id"]);
      unset($reservaArr["cliente_id"]);
      $funcion=Funcion::getById($reservaArr["funcion_id"]);
      unset($reservaArr["funcion_id"]);

      $reserva = reserva::instanciate($reservaArr);
      $reserva->has_one("Cliente",$cliente);
      $reserva->has_one("Funcion",$funcion);

      //$reserva->has_many("Sillas",);
      $r=$reserva->create();


    }

  public function insertarReserva($pago,$estadoPago,$precio,$cliente_id,$funcion_id,$sala,$pelicula_id,$sillasArray){

    $reservaArr["id"]= null;
    $reservaArr["forma_de_pago"]= $pago;
    $reservaArr["estado_pago"]= $estadoPago;
    $reservaArr["precio"]= $precio;
    $reservaArr["Cliente_id"]= $cliente_id;
    $reservaArr["Funcion_id"]= $funcion_id;

    $cliente=Cliente::getById($cliente_id);
    $sala = Sala::getById($sala);
    $pelicula = Pelicula::getById($pelicula_id);
    $funcion=Funcion::getById($funcion_id);

    $reserva = reserva::instanciate($reservaArr);
    $reserva->has_one("Cliente",$cliente);
    $reserva->has_one("Funcion",$funcion);

    //$reserva->has_many("Sillas",);
    $r=$reserva->create();
  //  $r=$reserva->update();
    $cantidadSilla = count($sillasArray);
  for ($i=0; $i < $cantidadSilla; $i++) {

    $sillaArr["id"]=null;
    $sillaArr["Reserva_id_reserva"]=null;
    $sillaArr["Silla_idSilla"]=null;
    $silla=Silla::getById($sillasArray[$i]);
    $reservaId=Reserva::getById($reserva->getId());
    $reservaSillas= reserva_has_Silla::instanciate($sillaArr);
    $reservaSillas->has_one("Silla",$silla);
    $reservaSillas->has_one("Reserva",$reservaId);


  $r2=$reservaSillas->create2();
    print_r($r2);


  # code...
}



  }

  public function update($reservaArr){

    $reservaArr["Cliente_id"]= null;
    $reservaArr["Funcion_id"]= null;


     $cliente=Cliente::getById($reservaArr["cliente_id"]);
     unset($reservaArr["cliente_id"]);
     $funcion=Funcion::getById($reservaArr["funcion_id"]);
     unset($reservaArr["funcion_id"]);


      $fun = Reserva::instanciate($reservaArr);


      $fun->setId($reservaArr["id"]);
      $fun->setForma_de_pago($reservaArr["forma_de_pago"]);
      $fun->setEstado_pago($reservaArr["estado_pago"]);
      $fun->setPrecio($reservaArr["precio"]);
      $fun->setCliente_id($cliente->getId());
      $fun->setFuncion_id($funcion->getId());

      $r = $fun->update("id",$fun->getId());
      return $r;



  }


  public function delete($id){

    $funDelete=Reserva::where("id",$id);
    $fun = Reserva::instanciate($funDelete[0]);
      //$r=Funcion::deleteRelationships2();
    $r=Reserva::mydelete($fun->getId());

  }


}
