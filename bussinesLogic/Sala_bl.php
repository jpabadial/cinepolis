<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Sala_bl {

  public function listarSalas(){
      $values= Sala::getAll();
      if(!empty($values)){
            return $values;
        }else{
            return "No hay Salas";
        }
  }

  public function getSala($id){
      //echo $id;
      $sala=Sala::getBy("id", $id);
      return($sala);

 }

 public function update($salaArr){


    $fun = Sala::instanciate($salaArr);
    $fun->setId($salaArr["id"]);
    $fun->setNumero($salaArr["numero"]);
    $fun->setCapacidad($salaArr["capacidad"]);
    $fun->setTeatro_id($salaArr["teatro_id"]);
    $fun->setTipo_id($salaArr["tipo_id"]);
    print_r($fun);
    $r = $fun->update("id",$fun->getId());
   print_r($r);
    //return $r;

 }

 public function delete($id){
   $funDelete=Sala::where("id",$id);
   $fun = Sala::instanciate($funDelete[0]);
   $r=Sala::mydelete($fun->getId());

 }

 public function save($salaArr){

   $idTeatro = $salaArr["teatro_id"];
   $idTipo_id = $salaArr["tipo_id"];
   $salaArr["id"] = null;
   $salaArr["Teatro_id"] = null;
   $salaArr["Tipo_id"] = null;

   $teatro = Teatro::getById($idTeatro);
   unset($salaArr["teatro_id"]);

   $tipoId = TipoSala::getById($idTipo_id);
   unset($salaArr["tipo_id"]);

   $fun = Sala::instanciate($salaArr);

   $fun->setNumero($salaArr["numero"]);
   $fun->setCapacidad($salaArr["capacidad"]);

    $fun->has_one("TipoSala",$tipoId);
    $fun->has_one("Teatro",$teatro);
    $r= $fun->create();
    return $r;


    }



}
