<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Teatro_bl {

  public function listarTeatros(){


      $teatros = Teatro::getAll();
      return $teatros;
      //$teatros= Teatro::getBy("Teatro_id", Session::get("Teatro_id"),true);
      //  echo  Session::get("Teatro_id");
      //  print_r($teatros);
        //return $teatros;
    }

    public function allTeatros(){
      $teatros = Teatro::getAll();
      return $teatros;
    }

    public function getTeatro($Teatro_id){
      Session::set("Teatro_id", $Teatro_id);
      $teatros= Teatro::getBy("Teatro_id", $Teatro_id,true);
      //print_r($teatros);
      print(json_encode($teatros));
  }

  public function update($teatroArr){

    $teatroArr["estado"]=1;

     $fun = Teatro::instanciate($teatroArr);
     $fun->setId($teatroArr["id"]);
     $fun->setNombre($teatroArr["nombre"]);
     $fun->setEstado($teatroArr["estado"]);
     $fun->setTelefono($teatroArr["telefono"]);
     $fun->setDireccion($teatroArr["direccion"]);
     $fun->setCiudad_id($teatroArr["ciudad_id"]);
     print_r($fun);
     $r = $fun->update("id",$fun->getId());
    print_r($r);
     //return $r;

  }

  public function delete($id){
    $funDelete=Teatro::where("id",$id);
    $fun = Teatro::instanciate($funDelete[0]);
    $r=Teatro::mydelete($fun->getId());

  }

  public function save($teatroArr){

    $idCiudad = $teatroArr["ciudad_id"];
    $teatroArr["id"] = null;
    $teatroArr["Ciudad_id"] = null;
    $teatroArr["estado"]= 1;

    $ciudad = Ciudad::getById($idCiudad);
    unset($teatroArr["ciudad_id"]);

    $fun = Teatro::instanciate($teatroArr);

    $fun->setNombre($teatroArr["nombre"]);
    $fun->setEstado($teatroArr["estado"]);
    $fun->setTelefono($teatroArr["telefono"]);
    $fun->setDireccion($teatroArr["direccion"]);

     $fun->has_one("Ciudad",$ciudad);
     $r= $fun->create();
     return $r;
     }




}
