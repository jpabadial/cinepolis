<?php

class Cliente_controller extends Controller{

    function __construct() {
        parent::__construct();
    }

    public function index(){
        $this->view->ciudades = Ciudad_bl::listarCiudades();
        $this->view->paises = Pais_bl::listarPaises();
        $this->view->peliculas = Peliculas_bl::listarPeliculas();
        $this->view->estrenos = Peliculas_bl::proximosEstrenos();
        $this->view->teatros = Teatro_bl::listarTeatros();

        if (Session::get("cliente_id") != null) {
          $this->view->cliente= Cliente_bl::accederCliente();
          $this->view->reservas= Reserva_bl::reservascliente(Session::get("cliente_id"));
           //print_r(Reserva::getBy("Cliente_id",Session::get("cliente_id")));
           $this->view->funciones=Funcion::getAll();
           $this->view->peliculas=Pelicula::getAll();

        }

        else{$this->view->cliente=null;}

        $this->view->render($this,"cliente","Perfil");

  }

     // Ciudades

    public function listarClientes(){
        print_r(Cliente_bl::listarClientes());
    }

    public function registro(){




        $this->validateKeys(["email","password","password1", "nombre", "apellido", "fechaNacimiento" , "telefono"], $_POST);
        if(filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
          if($_POST["password"] == $_POST["password1"]){
            $er = Cliente_bl::emailExistente($_POST["email"]);
            if($er==false){
              $nombre = $_POST["nombre"]." ".$_POST["apellido"];

              $r = Cliente_bl::registro($nombre,$_POST["password"],$_POST["tarjetaCinepolis"],$_POST["email"],$_POST["fechaNacimiento"], $_POST["telefono"]);
              if($r==true){
                header("location:".URL."Cliente/index");

              }

            }else{
              //el usuario ya existe
              //echo "el usuario ya existe, ingrese sesión";
              header("location:".URL."Login/LoginUser");
            }

          }else{
          //echo '<script language="javascript">alert("Lo siento, la contraseña ingresada no es la misma!");</script>';
          header("location:".URL."Login/LoginUser");
          }


        }else{
          header("location:".URL."Index/vistaLogin");
          //echo "Correo inválido";
        }
    }


        public function reserva(){


            $this->view->cliente= Cliente_bl::accederCliente();

            $pelicula_id =  Session::get("pelicula_id");
            $cliente_id =  Session::get("cliente_id");
            $pelicula=Peliculas_bl::buscarPeliculaPorid($pelicula_id);

            $peliculaNombre =$pelicula->getTitulo();
            $funcion = $_POST["funcion"];
            $hora = $_POST["hora"];
            $sala= $_POST["sala"];
            $cantidadSilla= $_POST["cantidadS"];
            $sillasArray;
            for ($i=0; $i < $cantidadSilla; $i++) {
              $a= $i+1;
              $valueSilla= $_POST["silla$a"];
              $sillasArray[$i]=$valueSilla;
            }
            $precio= $_POST["precio"];
            $pago= $_POST["pago"];

            if($pago=="Efectivo"){

              $estadoPago="Pendiente";
            }else{
              $estadoPago="Realizado";

            }

          //  echo $funcion." ".$hora." "." " .$sala." ".$cantidadSilla." ".$precio." ".$pago." ".$estadoPago,$cliente_id;

          $rRerserva=Reserva_bl::insertarReserva($pago,$estadoPago,$precio,$cliente_id,$funcion,$sala,$pelicula_id,$sillasArray);


          header("location:".URL."Cliente/index");

        }


    public function cerrarSesion(){

      Session::remove("cliente_id");
      Session::destroy();
      header("location:".URL."Index/index");
    }

    public function login()  {

      $this->validateKeys(["email","password"], $_POST);
      $r = $this->view->cliente = Cliente_bl::login($_POST["email"],$_POST["password"]);
      if($r){
        header("location:".URL."Cliente/index");
      }else{
      header("location:".URL."Index/index");

      }

    }


    public function logout()
    {
        Session::remove("id");
        Session::destroy();
        header("Location:".URL);
    }
}
