<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Index_controller extends Controller{

    function __construct() {
        parent::__construct();
    }

    public function index(){

        $this->view->estrenos = Peliculas_bl::proximosEstrenos();
        $this->view->paises = Pais_bl::listarPaises();
        $this->view->ciudades = Ciudad_bl::listarCiudades();
        $this->view->teatros = Teatro_bl::listarTeatros();
        //print_r($this->view->teatros);
        $this->view->peliculas = Peliculas_bl::listarPeliculas();
        if (Session::get("cliente_id") != null) { $this->view->cliente= Cliente_bl::accederCliente();}else{$this->view->cliente=null;}
        $this->view->render($this,"index","Cinepolis");
    }


    public function getCiudad(){
         $ciudades=Ciudad_bl::getCiudad($_POST["idPais"]);
         return($ciudades);
    }

   public function getTeatro(){

     if ( $_POST["idCiudad"] != "null") {
        $teatros=Teatro_bl::getTeatro($_POST["idCiudad"]);
           //print_r();
           //echo $_POST["idCiudad"];
           //echo $teatros;
           return($teatros);
         }else if( $_POST["idCiudad"] == "null"){
           //echo Session::get("ciudad_id");
           $teatros=Teatro_bl::getTeatro(Session::get("ciudad_id"));
           return($teatros);
         }


       }



}
