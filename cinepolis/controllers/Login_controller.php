<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Login_controller extends Controller{

    function __construct() {
        parent::__construct();
    }

    public function LoginUser(){
    $this->view->tipoDato = TipoIdentificacion_bl::obtenerTipos();
    $this->view->ciudades = Ciudad_bl::listarCiudades();
    $this->view->paises = Pais_bl::listarPaises();
    $this->view->teatros = Teatro_bl::listarTeatros();
    $this->view->peliculas = Peliculas_bl::listarPeliculas();
    if (Session::get("cliente_id") != null) { $this->view->cliente= Cliente_bl::accederCliente();}else{$this->view->cliente=null;}

     $this->view->render($this,"login","Login");
    }

    public function loginReserva(){
        if (Session::get("cliente_id") != null) { $this->view->cliente= Cliente_bl::accederCliente();
          echo  "true";
        }else{$this->view->cliente=null;
          echo  "false";
        }

    }


}
