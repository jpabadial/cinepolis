<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Pelicula_controller extends Controller{

    function __construct() {
        parent::__construct();
    }

    public function pelicula()
    {
        $this->view->peliculas = Peliculas_bl::listarPeliculas();
        $this->view->estrenos = Peliculas_bl::proximosEstrenos();

      //  $this->view->estrenos = Estrenos_bl::listarEstrenos();
    //    $this->view->peliculasVip = peliculasVip_bl::listarPeliculasVip();
     $this->view->render($this,"index","Cinepolis");
    }

}
