<?php include MODULE."head.php"; ?>

<body style="background-color: #0B5BA1;">

    <?php include MODULE."headerC.php"; ?>
    <?php include MODULE."menu.php"; ?>


    <div id="mainSliderB" >

  <div class="clientInfo">
    <div class="cajita" style="width:30%; display:flex; justify-content:center; align-items:center; align-content:center;">
      <div style="align-self:center; width:80%; "><a href="<?php print(URL)."Cliente/index"; ?>"><img id="loginButton"
        style="width:100%; border-radius: 10px 10px 10px 10px; -moz-border-radius: 10px 10px 10px 10px; -webkit-border-radius: 10px 10px 10px 10px;"
        src="<?php print(URL); ?>public/assets/images/fotoPerfil.png"alt=""></a></div>
    </div>
    <div class="cajita" style="width:50%; margin-left:2em;">
      <h1 style="color: #494949;  border-width: 0px 0px 4px 0px; border-style: solid; padding: 0em 0.5em 0.1em 0.5em; font-size:2.2em;"> Bienvenido a cinépolis ID</h1>
      <h2 style="color: #494949; font-weight:lighter; align-self:flex-start; font-size:2em;"><?php echo $this->cliente->getNombre();?></h2>
      <h3 style="color: #494949; font-weight:lighter; align-self:flex-start; font-size:1.2em; margin-top:-1%;"> ID: <?php echo $this->cliente->getEmail();?></h3>
      <br>
      <div class="" style="background-color:white; text-align:center; width:100%; "> <h3 style="font-size:1em; margin-top:1%; margin-bottom:1%;"> Al  día de hoy tienes: </h3></div>
      <div class="puntos">
      <div class="" style="background-color:#237BC7; width:50%;  height:100%; display:flex; justify-content:center; align-items:center; color: white; padding: 0em;"> Puntos Cinépolis:</div>
      <div class="" style="background-color:#0B5BA1; width:50%; height:100%; display:flex; justify-content:center; align-items:center; color: white;"> 0 puntos </div>
      </div>
      <br>
    </div>
    <div class="cajita" style="width:20%; margin-left:2em; padding: 2em 0em 0em 0em;">
      <div class="" style="background-color:#05559D;  padding:0.5em; text-align:center; width:100%;"> <h3 style="color:white;"> ¿Quieres recibir notificaciones de cinépolis?</h3></div>
      <div style="align-self:center; width:40%;  margin-top:1.5em; "><a href="<?php print(URL)."Cliente/index"; ?>"><img id="loginButton"
        style="width:100%; border-radius: 10px 10px 10px 10px; -moz-border-radius: 10px 10px 10px 10px; -webkit-border-radius: 10px 10px 10px 10px;"
        src="<?php print(URL); ?>public/assets/images/buzon.jpg"alt=""></a></div>

    </div>

  </div>

  <form class="centeredForm" style="" action="<?php print(URL)."Cliente/cerrarSesion"; ?>">
    <button type="submit" class="btn btn-primary " style="vertical-align: middle;">Cerrar Sesión</button>
  </form>
  <br>
</div>

<div class="contenedorFull" style="background-color:#FFFFFF; height:1.5%;"></div>

    <section id="reserva">
    <h1>Reserva</h1>
    </div>

<div class="container" >
  <table class="table"  style=" border-color: #59A9C2;  border-style: solid;  border-width: 2px 2px 2px 2px; background-color: #F8F8F8; color:#00587A;">
  <thead>
   <tr>
     <th>Forma de Pago</th>
     <th>Estado</th>
     <th>Precio</th>
     <th>Película</th>
     <th>Fecha</th>
     <th>Hora</th>
   </tr>
  </thead>
  <tbody>

    <?php foreach($this->reservas as $reserva): ?>
   <tr>
     <td><?php echo  $reserva["forma_de_pago"];  ?></td>
     <td><?php  echo  $reserva["estado_pago"]  ;?></td>
     <td><?php  echo  $reserva["precio"]  ;?></td>
     <td>
       <?php foreach($this->peliculas as $pelicula): foreach($this->funciones as $funcion): ?>
          <?php
          if ($reserva["Funcion_id"]===$funcion["id"]) {
            if($funcion["Pelicula_id"]==$pelicula["id"]){ echo  $pelicula["titulo"];}
          }?>
          <?php endforeach; endforeach;?>
    </td>

    <td>
       <?php foreach($this->funciones as $funcion):?>
           <?php if ($reserva["Funcion_id"]===$funcion["id"]) {echo  $funcion["fecha"];}?>
           <?php endforeach;?>
    </td>
    <td>
       <?php foreach($this->funciones as $funcion): ?>
            <?php if ($reserva["Funcion_id"]===$funcion["id"]) {echo  $funcion["hora"];} ?>
            <?php endforeach;?>
    </td>

   </tr>

  <?php endforeach ;?>
  <td>
    <?php if ($this->reservas == null) {echo  "No tiene reservas hasta el momento";}else{
   echo  " ";
    }?></td>
    <td>
      <?php if ($this->reservas == null) {echo  " ";}else{
     echo  " ";
      }?></td>
      <td>
        <?php if ($this->reservas == null) {echo  " ";}else{
       echo  " ";
        }?></td>
        <td>
          <?php if ($this->reservas == null) {echo  " ";}else{
         echo  " ";
          }?></td>
          <td>
            <?php if ($this->reservas == null) {echo  " ";}else{
           echo  " ";
            }?></td>
            <td>
              <?php if ($this->reservas == null) {echo  "   ";}else{
             echo  " ";
              }?></td>


  </tbody>





  </table>
</div>

<br>
<br>

  <script>
    $(document).ready(function() {
      $("#moviesSelected66").click(function(){
        $("#tituloMovie  ").click(function (){
                 $.post("<?php echo URL; ?>Funcion/getFunciones",{id:$(this).attr("value")},function(r){
                   var r = JSON.parse(r);
                   var c = $("#funcion");
                    if(r.length > 0){
                       c[0].disabled = false;
                       for (var i = 0; i < r.length; i++) {
                      console.log("mi id es"+r);
                       }
                     }
                   else{
                     alert("Error: "+r);
                   }
                 });
                });
       });
    });
  </script>


  <div class="moviesContainer">
    <div class="contenedorInicial">
      <div class="separador">Cartelera</div>
      <div class="movies">
          <?php foreach($this->peliculas as $pelicula): ?>
            <div class="movie" style=" background: url(<?php echo URL.$pelicula["poster"]; ?>); background-size:cover;">
              <movieInfo id="<?php echo $pelicula["id"]; ?>">
                <verCartelera  value="<?php echo $pelicula["titulo"]; ?>" class="movie-accions-a" style="margin-top:2.5em; padding-bottom:0.5em;"></verCartelera>
                <verSinopsis   value="<?php echo $pelicula["id"]; ?>" class="movie-accions-a" href="#">Ver sinopsis</verSinopsis>
                <br>
              </movieInfo>
                <div class="title"><?php echo $pelicula["titulo"]; ?></div>
            </div>
          <?php endforeach; ?>
      </div>
    </div>

  </div>

  <div class="moviesContainer">
    <div class="contenedorInicial">
              <div class="separador">Próximos Estrenos</div>
              <div class="movies">
                  <?php foreach($this->estrenos as $estreno): ?>
                  <div class="movie" style=" background: url(<?php echo URL.$estreno["poster"]; ?>);background-size:cover;">
                    <movieInfo id="<?php echo $pelicula["id"]; ?>">
                      <verCartelera  value="<?php echo $pelicula["titulo"]; ?>" class="movie-accions-a" style="margin-top:2.5em; padding-bottom:0.5em;"></verCartelera>
                      <verSinopsis   value="<?php echo $pelicula["id"]; ?>" class="movie-accions-a" href="#">Ver sinopsis</verSinopsis>
                      <br>
                    </movieInfo>
                      <div class="title"><?php echo $estreno["titulo"]; ?></div>
                  </div>
                  <?php endforeach; ?>
              </div>
    </div>
  </div>

            <script>
      $(document).ready(function() {
        $("verSinopsis").click(function (){
            var id = $(this).attr("value");
            console.log(id);
            window.location.replace(url+"Funcion/getFunciones/"+id);
          });
      });
      </script>


      <?php include MODULE."footer.php"; ?>



</body>
</hmtl>
