<?php include MODULE."head.php"; ?>

<body>

    <?php include MODULE."headerC.php"; ?>
    <?php include MODULE."menu.php"; ?>

    <div id="moviesContainer" style="display:flex; align-items:center; justify-content:center; width:100%;">
      <div class="contenedorCentro" style="width:90%; align-self:center; padding:0em;">
        <div class="peliFuncion">
          <div class="separador2">
            <h4>Cartelera</h4>
          </div>
          <div class="contenedorPeli">


              <div class="pelicula" style=" background: url(<?php echo URL.$this->pelicula->getPoster(); ?>);background-size:cover;">

              </div>

              <div class="infoPeli" style=" align-self: center;flex-flow: row nowrap; width:70%; margin:0%; padding: 2%; 2% 2% 0.1%;">

                <div class="infoPeli" style="width:100%;">
                  <div class="datosPeli">
                    <div class="datosPeli2" style="  width: 100%;border-width: 0px 0px 0px 0px; border-style: solid; padding: 0em 0em 0.1em 0em; color: #0B5BA1;">
                      <h1 style="border-width: 0px 0px 0px 0px; border-style: solid; color: #0B5BA1; text-align:left;"><?php echo $this->pelicula->getTitulo(); ?></h1>
                    </div>
                </div>
                <br>
                  <div class="datosPeli" style="width:100%; display:flex; flex-flow:row nowrap;">
                    Subtitulada:
                    <div class="datosPeli2" style="padding:0em;">
                      <?php
                      $respuesta1=1;
                    $respuesta2=2; $subtitulada=$this->pelicula->getSubtitulada();
                    if($subtitulada==$respuesta1){
                    echo "Tiene subtítulos";
                    }
                    if($subtitulada==$respuesta2){
                    echo "No tiene subtítulos";
                    }
                      ?>
                    </div>
                </div>
                  <div class="datosPeli" style="width:100%; display:flex; flex-flow:row nowrap;">
                    Categoria:
                    <div class="datosPeli2">
                      <?php
                      echo $this->categoria->getNombre();

                      ?>
                    </div>
                  </div>

                  <div class="datosPeli" style="width:100%; display:flex; flex-flow:row nowrap;">
                    Fecha de estreno:
                    <div class="datosPeli2">
                      <?php
                      echo $this->pelicula->getFecha_estreno();

                      ?>
                    </div>
                  </div>

                </div>

                <br>

                <div class="datosPeli" style="padding:1em; width:130%;">
                  Sinopsis:
                  <div class="datosPeli2" style="padding:2em; text-align:justify;">
                    <?php
                    echo $this->pelicula->getSinopsis();

                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="listaFuncion">
            <div class="separadorFuncion"><h4 style="text-align:center; padding: 0em 0em 0.5em 0em;">Funciones para esta película: </h4></div>
            <br>
            <?php
//print_r($this->funciones);
            foreach($this->funciones as $funcion):?>
            <div class="contenedorPeli2">



                <div class="infoPeli">
                    <div class="datosPeli">
                    Fecha:
                    <div class="datosPeli2">
                      <?php
                      echo $funcion["fecha"];
                      ?>
                    </div>
                    </div>
                  <div class="datosPeli">
                    Hora:
                    <div class="datosPeli2">
                      <?php
                      echo $funcion["hora"];
                      ?>
                    </div>
                  </div>
                  <div class="datosPeli">
                    Sala:
                    <div class="datosPeli2">
                      <?php
                    echo $funcion["sala"]["numero"];

                      ?>
                    </div>
                  </div>

                </div>


              </div>
            <?php endforeach;?>


            <div class="reservarBoton">
              <button id="butonReserva" type="button" style="border-color: #0B5BA1;  border-style: solid;  border-width: 5px 5px 5px 5px; background-color: #F8F8F8; color:#00587A; width: 30%; align-self:center; color: #0B5BA1; font-weight: bolder;" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg">Reservar</button>


              <script>
                 $(document).ready(function() {
                   $("#butonReserva").click(function(){
                         $.post("<?php echo URL;?>Login/loginReserva",{},function (r) {
                           console.log("entre:"+r);
                           if(r=="false"){
                             alert("Recuerde que antes de hacer una reserva debe inciar sesión");
                             window.location.replace("<?php echo URL;?>Login/LoginUser");
                           }

                      });
                    });
                 });
                 </script>

              <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Realizar Reserva</h4>
                      </div>
                      <div class="modal-body">
                      <form class="centeredForm"  action="<?php echo URL."Cliente/reserva"  ?>"  method="post">

                      <div class="formularioColumna">
                        <div class="form-group" style="margin-right:1em; width:70%;">
                          <label for="">Pelicula: </label>

                        <select id="peliculas"  name="pelicula"class="form-control">
                        <option value="<?php echo $this->pelicula->getId(); ?>">
                        <?php echo $this->pelicula->getTitulo();?>
                        </option>

                        </select>
                        </div>

                        <div class="form-group" style="margin-right:1em; width:70%;">
                        <label for="">Función: </label>
                        <select id="funcion"  name="funcion"class="form-control">
                        <option>
                        Seleccione una función
                        </option><?php  foreach($this->funciones as $funcion):?>
                        <option value="<?php echo $funcion["id"]; ?>">
                        <?php echo $funcion["fecha"]; ?>
                        </option>
                        <?php endforeach;?>
                        </select>
                      </div>
                    </div>

                    <div class="formularioColumna">

                    <div class="form-group" style="margin-right:1em; width:40%;">
                      <label for="">Hora: </label>
                    <select id="hora"  name="hora"class="form-control" disabled="true">

                    </select>
                    </div>

                    <div class="form-group" style="margin-right:1em; width:40%;">
                      <label for="">Sala: </label>
                    <select id="sala"  name="sala"class="form-control" disabled="true">

                    </select>
                    </div>

                    <div class="form-group" style="margin-right:1em; width:40%;">
                      <label for="">¿Cuantas sillas va a reservar?</label>
                    <select id="cantidadS"  name="cantidadS"class="form-control" disabled="true" >

                      <option value="1">1</option>
                       <option value="2">2</option>
                       <option value="3">3</option>
                       <option value="4">4</option>
                       <option value="5">5</option>
                    </select>
                    </div>

                    </div>
                      <div class="form-group" style="margin-right:1em; width:70%; align-self:flex-start;">
                        <label for="">Seleccione las sillas que desea reservar </label>
                        <div class="checkbox" id="silla"></div>
                      </div>

                    <div class="formularioColumna">

                      <div class="form-group" style="margin-right:1em; width:70%;">
                        <label for="">Precio a pagar: </label>
                      <textarea id="precio" name="precio"class="form-control" rows="1"disabled="true"></textarea>
                      </div>

                      <div class="form-group" style="margin-right:1em; width:70%;">
                        <label for="">Forma de Pago</label>
                      <select id="pago"  name="pago"class="form-control"

                       disabled="true" >
                        <option value="Efectivo">Efectivo en Taquilla</option>
                         <option value="Debito">Debito</option>
                         <option value="Credito">Credito</option>

                      </select>
                      </div>

                    </div>


    <script>

    $(document).ready(function() {
      function obtenerSilla(idSala){
        var sala=idSala;
          var c = $("#silla");
          $(c).find("label").remove();
        $.post("<?php echo URL; ?>Silla/getSillas",
          {
            id:sala
          },function(r){
            console.log(r);
              if(JSON.parse(r)){
                var r = JSON.parse(r);
                var c = $("#silla");
                  if(r.length > 0){

                    for (var i = 0; i < r.length; i++) {

                      if(r[i]["estado"]==1){

                        var label = document.createElement("label");
                          var silla = document.createElement("div");
                        var input = document.createElement("input");

                        input.name = "silla" + r[i]["id"];
                        input.value = r[i]["id"];
                        input.type = "checkbox";
                        input.onclick=function() {


                        document.getElementById("precio").disabled=false;


                        var elemento=this;
                        var contador=0;
                        var max=document.getElementById("cantidadS").value;
                          $("#cantidadS").change(function(){
                            max=this.value;
                          });
                        var precioBoleta=0;

                          $("input[type=checkbox]").each(function(){

                            if($(this).is(":checked"))
                              contador++;
                              precioBoleta=contador*8000;

                          });
                          var obj = document.getElementById("precio");
                          var txt =document.createTextNode(""+precioBoleta);
                          if (document.getElementById("precio").value=="") {
                            document.getElementById("pago").disabled=false;
                            obj.appendChild(txt);
                            }else{

                            var precio = $("#precio").val("");
                            var precio = $("#precio").val(precioBoleta);

                            }
                                      console.log(contador);

                          if(contador>max){
                            alert("Has seleccionado mas sillas que las indicadas");
//quitamos check del ultimo input
                            $(elemento).prop('checked', false);
                            contador--;
                          }
                          document.getElementById("precio").disabled=false;

//  var precio = $("#precio").val("");;

                          }
                          var numero =r[i]["numero"];
                          var fila =r[i]["fila"];
                          var total=numero+fila;
                          silla.innerHTML =total
                          silla.style="margin-right:26px;";
                          label.style="display:flex; flex-flow:row nowrap;";
                          document.getElementById("silla").style="display:flex; flex-flow:row nowrap; justify-content:flex-start;";
                          label.append(silla);
                          label.append(input);
                          c.append(label);
                        }
                      }
                    }
                  }else{
                    alert("Error: "+r);
                  }
                });
              }

              $("#funcion").change(function(){
              var c = $("#hora");
              var d = $("#sala");

              $(c).find("option").remove();
              $(d).find("option").remove();
              //  c[0].disabled=false;

              $.post("<?php echo URL; ?>Funcion/getDatosFuncion",
                  {
                  id:$(this).val()
                  },function(r){
                    if(JSON.parse(r)){
                      var r = JSON.parse(r);
                      var c = $("#hora");
                      var d = $("#sala");
                      var s = $("#cantidadS");

                      if(r.length > 0){
                        c[0].disabled = false;
                        d[0].disabled = false;
                        s[0].disabled = false;
                          for (var i = 0; i < r.length; i++) {
                            var el = document.createElement("option");
                            var el2 = document.createElement("option");
                            el.value = r[i]["hora"];
                            el.innerHTML = r[i]["hora"];
                            c.append(el);

                            el2.value = r[i]["Sala_id"];
                            el2.innerHTML = r[i]["Sala_id"];

                            var sala = obtenerSilla(r[i]["Sala_id"]);
                            console.log(sala);

                            d.append(el2);
                          }
                        }
                      }else{
                        alert("Error: "+r);
                      }
                    });
                  });
                });
                </script>

                      <button type="submit" class="btn btn-primary" style="vertical-align: middle;">Entrar</button>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            </div>


      </div>

    </div>
      <?php include MODULE."footer.php"; ?>



</body>
</hmtl>
