<?php include MODULE."head.php"; ?>

<body>

    <?php include MODULE."headerC.php"; ?>
    <?php include MODULE."menu.php"; ?>

    <div id="mainSlider">

    </div>

    <script type="text/javascript">
     url = "<?php echo URL; ?>";
    </script>

<div class="moviesContainer">
  <div class="contenedorInicial">
    <div class="separador">Cartelera</div>
    <div class="movies">
        <?php foreach($this->peliculas as $pelicula): ?>
          <div class="movie" style=" background: url(<?php echo URL.$pelicula["poster"]; ?>); background-size:cover;">
            <movieInfo id="<?php echo $pelicula["id"]; ?>">
              <verCartelera  value="<?php echo $pelicula["titulo"]; ?>" class="movie-accions-a" style="margin-top:2.5em; padding-bottom:0.5em;"></verCartelera>
              <verSinopsis   value="<?php echo $pelicula["id"]; ?>" class="movie-accions-a" href="#">Ver sinopsis</verSinopsis>
              <br>
            </movieInfo>
              <div class="title"><?php echo $pelicula["titulo"]; ?></div>
          </div>
        <?php endforeach; ?>
    </div>
  </div>

</div>

<div class="moviesContainer">
  <div class="contenedorInicial">
            <div class="separador">Próximos Estrenos</div>
            <div class="movies">
                <?php foreach($this->estrenos as $estreno): ?>
                <div class="movie" style=" background: url(<?php echo URL.$estreno["poster"]; ?>);background-size:cover;">
                  <movieInfo id="<?php echo $pelicula["id"]; ?>">
                    <verCartelera  value="<?php echo $estreno["titulo"]; ?>" class="movie-accions-a" style="margin-top:2.5em; padding-bottom:0.5em;"></verCartelera>
                    <verSinopsis   value="<?php echo $estreno["id"]; ?>" class="movie-accions-a" href="#">Ver sinopsis</verSinopsis>
                    <br>
                  </movieInfo>
                    <div class="title"><?php echo $estreno["titulo"]; ?></div>
                </div>
                <?php endforeach; ?>
            </div>
  </div>
</div>

      <?php include MODULE."footer.php"; ?>

          <script>
    $(document).ready(function() {
      $("verSinopsis").click(function (){
          var id = $(this).attr("value");
          console.log(id);
          window.location.replace(url+"Funcion/getFunciones/"+id);
        });
    });
    </script>



</body>
</hmtl>
