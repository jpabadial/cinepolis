
<?php include MODULE."head.php"; ?>

<body>

    <?php include MODULE."headerC.php"; ?>
    <?php include MODULE."menu.php"; ?>

<div class="moviesContainer">
  <div class="contenedorCentro">
    <h1 style="	font-size: 2em;
    	color:  #0f5da5;
    	font-family: sans-serif;
    	font-weight: bolder;">Cinépolis ID</h1>
    <h2 style="
    	font-size: 1.5em;
    	color:  #0f5da5;
    	font-family: sans-serif;
    	font-weight: bolder;"> - Es tu clave de identificación única en Cinépolis</h2>
    <h3 style="font-size: 1.2em;
  	color: #566165;
  	font-family: sans-serif;
  	font-weight: inherit;
  	margin: 0em;
  	padding: 0.15em;">Con ella podrás: </h3>
    <h3 style="font-size: 1.2em;
  	color: #566165;
  	font-family: sans-serif;
  	font-weight: inherit;
  	margin: 0em;
  	padding: 0.15em;">-consultar tu saldo e historial de tu TCC</h3>
    <h3 style="font-size: 1.2em;
  	color: #566165;
  	font-family: sans-serif;
  	font-weight: inherit;
  	margin: 0em;
  	padding: 0.15em;">-Tener acceso a promociones</h3>
    <h3 style="font-size: 1.2em;
  	color: #566165;
  	font-family: sans-serif;
  	font-weight: inherit;
  	margin: 0em;
  	padding: 0.15em;">-Suscribirte a la cartelera</h3>
    <br>
    <button  name="login" type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">LOGIN</button>

  </div>

</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-content">
        <div class="modal-header">
          <button name="iniciar" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">LOGIN</h4>
        </div>
        <div class="modal-body">
            <form class="centeredForm" action="<?php print(URL); ?>Cliente/login" method="post">
              <div class="form-group">
                <label for="">Email: </label>
                <input type="email" name="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password">
              </div>
              <h5><a href="#" style="color: #566165;">¿Olvidaste tu contraseña?</a></h5>
              <button type="submit" class="btn btn-primary " style="vertical-align: middle;">Entrar</button>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>

<br>

<div class="contenedorFull" style="background-color:#252525; height:1.5%;"></div>

<div class="moviesContainer">
    <div class="contenedorCentro">
      <div style="margin-left: 25%; width:60%;">
        <h3 style="background-color:#237bc7; color: white; padding:0.5em 0.5em 0.5em 1em; text-align: left;">Registro</h3>
        <form style="background-color:#47494c; padding:1em;" action="<?php print(URL); ?>Cliente/registro" method="post">
          <div class="formularioColumna">
            <div class="form-group" style="margin-right:1em; width:70%;">
              <label for="exampleInputEmail1" style="color:white; font-weight:inherit; font-family:sans-serif;">Cinépolis ID</label>
              <input type="email"  name="email" class="form-control" id="emailR" placeholder="Ej: nombre@correo.com">
            </div>
            <button type="submit" class="btn btn-primary" style=" width:30% ;height:10%; align-self:center; margin-top:3%;"> ? </button>
          </div>
          <div class="formularioColumna">
            <div class="form-group" style="margin-right:1em;  width:50%">
              <label for="exampleInputPassword1" style="color:white; font-weight:inherit; font-family:sans-serif;">Contraseña:</label>
              <input type="password" name="password" class="form-control" id="passwordR" placeholder="Password">
            </div>
            <div class="form-group" style=" width:50%">
              <label for="exampleInputPassword1" style="color:white; font-weight:inherit; font-family:sans-serif;">Confirma contraseña:</label>
              <input type="password" name ="password1" class="form-control" id="passwordR1" placeholder="Password">
            </div>
          </div>

          <div class="formularioColumna">
            <div class="form-group" style="margin-right:1em; width:50%;">
              <label for="exampleInputPassword1" style="color:white; font-weight:inherit; font-family:sans-serif;">Nombre</label>
              <input type="text" name="nombre" class="form-control" id="nombreR" placeholder="Nombre">
            </div>
            <div class="form-group" style="width:50%;">
              <label for="exampleInputPassword1" style="color:white; font-weight:inherit; font-family:sans-serif; width:50%;">Apellido parterno:</label>
              <input type="text" name="apellido" class="form-control" id="apellidoR" placeholder="Apellido Paterno">
            </div>
          </div>

          <div class="formularioColumna">
            <div class="form-group" style= "width:50%;">
              <label for="" style="color:white; font-weight:inherit; font-family:sans-serif;">Fecha nacimiento</label>
              <input type="date" name="fechaNacimiento" class="form-control" id="fechaNacimientoR" placeholder=""  style=" width:90%;">
            </div>
            <div class="form-group" style="width:50%;">
              <label for="exampleInputPassword1" style="color:white; font-weight:inherit; font-family:sans-serif;">Teléfono</label>
              <input type="text" name="telefono" class="form-control" id="telefonoR" placeholder="(315) 4856743" style=" width:100%;">
            </div>
          </div>


          <div class="formularioColumna">
            <div class="form-group" style="margin-right:1em; width:70%;">
              <label for="exampleInputPassword1" style="color:white; font-weight:inherit; font-family:sans-serif;">Tarjeta club cinépolis</label>
              <input type="text" name="tarjetaCinepolis" class="form-control" id="exampleInputPassword1" placeholder="Introduce una sí tienes">
            </div>
            <button type="submit" class="btn btn-primary" style=" width:40% ;height:10%; align-self:center; margin-top:3%;">Enviar</button>
          </div>
        </form>
      </div>
</div>
</div>

<br>
<br>
<br>
      <?php include MODULE."footer.php"; ?>

</body>
</hmtl>
