
  <div class="footer">

    <div class="catalogoPeliculas">
      <h2 style="color:#FEC606;">Catá logo de películas</h2>
      <br>
      <h4 > Busca una película</h4>
      <div class="form-group">

      <select id="peliculas"  name="pelicula"class="form-control">
        <option value="">Seleccione Película</option>
      <?php foreach($this->peliculas as $pelicula): ?>
      <option value="<?php echo $pelicula["id"]; ?>">
      <?php echo $pelicula["titulo"]; ?>
      </option>
      <?php endforeach;?>
      </select>
      </div>
    </div>
    <div class="seccionPais">
      <h2 style="color: #FEC606;">Secció n País</h2>
      <br>
      <h4> Busca tu pais</h4>
      <div class="form-group">

      <select id="paises"  name="pais"class="form-control">
      <?php foreach($this->paises as $pais): ?>
        <option value="<?php echo $pais["id"];  ?>"> <?php echo $pais["nombre"];  ?></option>
      <?php endforeach;?>
      </select>


            <script>
            $(document).ready(function() {
              $("#paises").change(function(){
                    $.post("<?php echo URL;?>Index/getCiudad",{idPais:$(this).val()},function (r) {
                      //console.log(r);
                      if(JSON.parse(r)){
                      var r = JSON.parse(r);
                      //console.log(r);
                      var teatro = $("#teatro");
                      var teatroO = $(teatro).find("option");
                      teatro[0].disabled = true;
                      var c = $("#ciudad");
                      var options = $(c).find("option");
                      options.remove();
                      if(r.length > 0){
                        //c[0].disabled = false;
                        for (var i = 0; i < r.length; i++) {
                          var opciones = document.getElementsByClassName('optionsCiudades');
                          var el = document.createElement("option");
                          el.class = "optionsCiudades";
                          el.value = r[i]["id"];
                          el.innerHTML = r[i]["nombre"];
                          c.append(el);
                        }
                      }
                    }else{
                      alert("Error: "+r);
                    }
                 });
                 $.post("<?php echo URL;?>Index/getTeatro",{idCiudad:"null"},function (r) {
                   //console.log(r);
                   if(JSON.parse(r)){
                   var r = JSON.parse(r);
                   //console.log(r);
                   var c = $("#teatro");
                   //console.log(r);
                   var options = $(c).find("option");
                   options.remove();
                   var el = document.createElement("option");
                   el.innerHTML = "seleccione un cine";
                   c.append(el);
                   if(r.length > 0){
                     //c[0].disabled = false;
                     for (var i = 0; i < r.length; i++) {
                       var el = document.createElement("option");
                       el.value = r[i]["id"];
                       el.innerHTML = r[i]["nombre"];
                       c.append(el);
                     }
                   }
                 }else{
                   alert("Error: "+r);
                 }
              });
               });
            });
            </script>



      </div>
        </div>
    </div>
  </div>
