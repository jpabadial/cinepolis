-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2017 a las 01:12:51
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cinepolis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(5) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`) VALUES
(1, 'luisa', '1234'),
(2, 'root', 'rooty');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriapelicula`
--

CREATE TABLE `categoriapelicula` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoriapelicula`
--

INSERT INTO `categoriapelicula` (`id`, `nombre`) VALUES
(1, 'Acció n'),
(2, 'Terror'),
(3, 'Suspenso'),
(4, 'Comedia'),
(5, 'Romance'),
(7, 'Terror más drama');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `Pais_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id`, `nombre`, `Pais_id`) VALUES
(1, 'Cali', 1),
(2, 'Bogotá', 1),
(3, 'Buenos Aires', 2),
(7, 'Medellín', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `contrasena` varchar(45) NOT NULL,
  `identificacion` int(11) NOT NULL,
  `tipo_identificacion_id` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `nombre`, `contrasena`, `identificacion`, `tipo_identificacion_id`, `email`, `fechaNacimiento`, `telefono`) VALUES
(29, 'luisa gon', '1234', 5346346, 3, 'luisabgv@gmail.com', '1999-09-16', 2147483647),
(30, 'Luisa González', '1234', 0, 3, 'luisabgv@hotmail.com', '2017-03-09', 2147483647),
(31, 'Pedrito Petroncio', '1234', 0, 3, 'Pedrito@pedrito.co', '2017-03-17', 2147483647),
(33, 'Lina', '1234', 0, 3, 'lina@gmail.com', '2017-03-28', 2147483647),
(34, 'Juan Guillermo Bernal', '1234', 0, 3, 'juan@gmail.com', '2016-01-14', 2147483647);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcion`
--

CREATE TABLE `funcion` (
  `id` int(11) NOT NULL,
  `hora` varchar(5) NOT NULL,
  `fecha` date NOT NULL,
  `Sala_id` int(11) NOT NULL,
  `Pelicula_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `funcion`
--

INSERT INTO `funcion` (`id`, `hora`, `fecha`, `Sala_id`, `Pelicula_id`) VALUES
(4, '09:00', '2017-03-23', 1, 4),
(5, '21:02', '2017-04-02', 1, 4),
(6, '20:04', '2017-04-03', 1, 42),
(7, '21:22', '2017-04-10', 2, 38),
(8, '19:22', '2017-04-11', 1, 43),
(9, '19:22', '2017-04-04', 2, 44),
(10, '17:26', '2017-04-04', 2, 45);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `nombre`) VALUES
(1, 'Colombia'),
(2, 'Argentina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `id` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `Sinopsis` varchar(2000) NOT NULL,
  `subtitulada` tinyint(1) NOT NULL,
  `poster` varchar(45) DEFAULT NULL,
  `fecha_estreno` date NOT NULL,
  `Categoria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`id`, `titulo`, `Sinopsis`, `subtitulada`, `poster`, `fecha_estreno`, `Categoria_id`) VALUES
(4, 'Logan', 'Es el año 2029. Los mutantes prácticamente han desparecido. Un cansado y abatido Logan (Hugh Jackman) vive retirado en la ciudad mexicana de El Paso. Es una sombra de lo que era. Se gana la vida conduciendo limusinas y se emborracha más de la cuenta. Su compañero en el exilio es el Profesor Charles Xavier (Patrick Stewart), también en las últimas, ya que está enfermo, inválido y con sus facultades mentales deterioradas. Logan cuida de él. Pero los intentos de Logan por ocultarse del mundo y olvi', 1, 'public/assets/images/4.jpg', '2017-02-28', 1),
(38, 'Zootopia', 'Se trata de una película "buddy cop" («película de colegas»5 ) En un mundo poblado de mamíferos antropomórficos donde depredadores y especies de presas coexisten pacíficamente, una coneja de Bunnyburrow rural llamada Judy Hopps cumple con gran esfuerzo su sueño de niñez de convertirse en la primera coneja oficial de policía de la utopía urbana, Zootopia. A pesar de salir de la academia de policía con grandes honores, Judy es relegado al deber de estacionamiento por Jefe Bogo, quien duda de su potencial. En su primer día, ella es estafada por Nick Wilde y Finnick, un dúo estafador de zorros.', 1, 'public/assets/images/38.jpg', '2017-04-03', 1),
(39, 'Rápido y furioso', '2017-04-03', 1, 'public/assets/images/39.jpg', '2017-04-03', 1),
(42, 'Lala land', 'La película cuenta la historia de Mia, una camarera de Hollywood que aspira a ser una gran actriz y Sebastian, un pianista de jazz desempleado con grandes ambiciones. A pesar de sus diferencias y sus distintas personalidades, gracias a una serie de acontecimientos harán que sus caminos acaben cruzándose.', 1, 'public/assets/images/42.jpg', '2017-04-03', 5),
(43, 'Backseat fighter', 'Backseat Fighter es la historia de Mark, un luchador que es transportado en el asiento trasero de un coche para luchar en combates clandestinos. De camino a uno de esas luchas se encuentra con Sandy, una joven y hermosa prostituta que comparte el mismo deseo que él: dejar atrás todo el dolor y comenzar desde cero en una nueva vida. Sin embargo, Mark está a punto de enfrentarse cara a cara con su pasado, del que no ha parado de huir, lo que pone en peligro ese plan de comenzar de nuevo, lo que incluye a Sandy y a todos los que le rodean. ', 1, 'public/assets/images/43.jpg', '2017-04-02', 3),
(44, 'RockDog', '2017-04-01', 2, 'public/assets/images/44.jpg', '2017-04-01', 4),
(45, 'La bella y la bestia', 'Una joven soñadora y romántica llamada Bella (Emma Watson) vive en una pequeña aldea junto a su padre, un inventor de artilugios, al que algunos consideran un anciano demente. ... Buscando cobijo y un lugar donde pasar la noche, el padre de Bella descubre que el señor de ese castillo es una horrible Bestia (Dan Stevens).', 1, 'public/assets/images/45.jpg', '2017-04-04', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `id` int(11) NOT NULL,
  `forma_de_pago` varchar(45) NOT NULL,
  `estado_pago` varchar(45) NOT NULL,
  `precio` float NOT NULL,
  `Cliente_id` int(11) NOT NULL,
  `Funcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`id`, `forma_de_pago`, `estado_pago`, `precio`, `Cliente_id`, `Funcion_id`) VALUES
(110, 'Debito', 'Realizado', 8000, 29, 4),
(119, 'Debito', 'Realizado', 16000, 29, 4),
(120, 'Efectivo', 'Pendiente', 16000, 29, 4),
(121, 'Credito', 'Realizado', 16000, 29, 4),
(123, 'Credito', 'Realizado', 16000, 30, 5),
(131, 'Debito', 'Realizado', 8000, 34, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_has_silla`
--

CREATE TABLE `reserva_has_silla` (
  `id` int(5) NOT NULL,
  `Reserva_id_reserva` int(11) NOT NULL,
  `Silla_idSilla` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reserva_has_silla`
--

INSERT INTO `reserva_has_silla` (`id`, `Reserva_id_reserva`, `Silla_idSilla`) VALUES
(44, 103, 1),
(45, 103, 2),
(46, 104, 1),
(47, 105, 1),
(48, 106, 1),
(49, 107, 1),
(50, 108, 1),
(51, 109, 1),
(52, 110, 1),
(53, 119, 1),
(54, 120, 1),
(55, 121, 1),
(56, 123, 1),
(57, 131, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sala`
--

CREATE TABLE `sala` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `Teatro_id` int(11) NOT NULL,
  `Tipo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sala`
--

INSERT INTO `sala` (`id`, `numero`, `capacidad`, `Teatro_id`, `Tipo_id`) VALUES
(1, 1, 120, 1, 1),
(2, 2, 150, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `silla`
--

CREATE TABLE `silla` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `fila` varchar(2) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `Sala_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `silla`
--

INSERT INTO `silla` (`id`, `numero`, `fila`, `estado`, `Sala_id`) VALUES
(1, 1, 'A', 1, 1),
(3, 2, 'A', 1, 1),
(4, 3, 'A', 1, 1),
(5, 4, 'A', 1, 1),
(6, 5, 'A', 1, 2),
(7, 1, 'B', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teatro`
--

CREATE TABLE `teatro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `telefono` int(11) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `Ciudad_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `teatro`
--

INSERT INTO `teatro` (`id`, `nombre`, `estado`, `telefono`, `direccion`, `Ciudad_id`) VALUES
(1, 'Cinepolis', 1, 3854679, 'Centro Comercial Limonar', 1),
(2, 'Cinepolis VIP', 1, 234789, 'Centro Comercial Limonar', 1),
(3, 'Teatro bogotá', 1, 543467, 'Hola Soy bogotá', 2),
(4, 'Teatro BuenosA', 1, 6543254, 'Autopista argentina', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoidentificacion`
--

CREATE TABLE `tipoidentificacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoidentificacion`
--

INSERT INTO `tipoidentificacion` (`id`, `nombre`) VALUES
(3, 'Tarjeta cinépolis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposala`
--

CREATE TABLE `tiposala` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tiposala`
--

INSERT INTO `tiposala` (`id`, `nombre`) VALUES
(1, 'VIP'),
(2, 'General');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoriapelicula`
--
ALTER TABLE `categoriapelicula`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Ciudad_Pais1_idx` (`Pais_id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_Cliente_tipo_identificacion1_idx` (`tipo_identificacion_id`);

--
-- Indices de la tabla `funcion`
--
ALTER TABLE `funcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_funcion_Sala1_idx` (`Sala_id`),
  ADD KEY `fk_funcion_Movie1_idx` (`Pelicula_id`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Pelicula_Categoria1_idx` (`Categoria_id`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Reservas_Cliente1_idx` (`Cliente_id`),
  ADD KEY `fk_Reservas_funcion1_idx` (`Funcion_id`);

--
-- Indices de la tabla `reserva_has_silla`
--
ALTER TABLE `reserva_has_silla`
  ADD PRIMARY KEY (`Reserva_id_reserva`,`Silla_idSilla`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_Reservas_has_Silla_Silla1_idx` (`Silla_idSilla`),
  ADD KEY `fk_Reservas_has_Silla_Reservas1_idx` (`Reserva_id_reserva`);

--
-- Indices de la tabla `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Sala_Teatro1_idx` (`Teatro_id`),
  ADD KEY `fk_Sala_Tipo1_idx` (`Tipo_id`);

--
-- Indices de la tabla `silla`
--
ALTER TABLE `silla`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Silla_Sala1_idx` (`Sala_id`);

--
-- Indices de la tabla `teatro`
--
ALTER TABLE `teatro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Teatro_Ciudad1_idx` (`Ciudad_id`);

--
-- Indices de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tiposala`
--
ALTER TABLE `tiposala`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `categoriapelicula`
--
ALTER TABLE `categoriapelicula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `funcion`
--
ALTER TABLE `funcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT de la tabla `reserva_has_silla`
--
ALTER TABLE `reserva_has_silla`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT de la tabla `sala`
--
ALTER TABLE `sala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `silla`
--
ALTER TABLE `silla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `teatro`
--
ALTER TABLE `teatro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tiposala`
--
ALTER TABLE `tiposala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `fk_Ciudad_Pais1` FOREIGN KEY (`Pais_id`) REFERENCES `pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_Cliente_tipo_identificacion1` FOREIGN KEY (`tipo_identificacion_id`) REFERENCES `tipoidentificacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `funcion`
--
ALTER TABLE `funcion`
  ADD CONSTRAINT `fk_funcion_Movie1` FOREIGN KEY (`Pelicula_id`) REFERENCES `pelicula` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_funcion_Sala1` FOREIGN KEY (`Sala_id`) REFERENCES `sala` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD CONSTRAINT `fk_Pelicula_Categoria1` FOREIGN KEY (`Categoria_id`) REFERENCES `categoriapelicula` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `fk_Reservas_Cliente1` FOREIGN KEY (`Cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Reservas_funcion1` FOREIGN KEY (`Funcion_id`) REFERENCES `funcion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reserva_has_silla`
--
ALTER TABLE `reserva_has_silla`
  ADD CONSTRAINT `fk_Reservas_has_Silla_Reservas1` FOREIGN KEY (`Reserva_id_reserva`) REFERENCES `reserva` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Reservas_has_Silla_Silla1` FOREIGN KEY (`Silla_idSilla`) REFERENCES `silla` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sala`
--
ALTER TABLE `sala`
  ADD CONSTRAINT `fk_Sala_Teatro1` FOREIGN KEY (`Teatro_id`) REFERENCES `teatro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Sala_Tipo1` FOREIGN KEY (`Tipo_id`) REFERENCES `tiposala` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `silla`
--
ALTER TABLE `silla`
  ADD CONSTRAINT `fk_Silla_Sala1` FOREIGN KEY (`Sala_id`) REFERENCES `sala` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `teatro`
--
ALTER TABLE `teatro`
  ADD CONSTRAINT `fk_Teatro_Ciudad1` FOREIGN KEY (`Ciudad_id`) REFERENCES `ciudad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
