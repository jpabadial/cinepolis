<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Admin extends Model {

  protected static $table = "admins";

  private $id;
  private $username;
  private $password;

  function __construct($id, $username, $password) {
      $this->id = $id;
      $this->username = $username;
      $this->password = $password;
  }

  public function getMyVars(){
      return get_object_vars($this);
  }

  static function getTable() {
      return self::$table;
  }

  function getId() {
      return $this->id;
  }

  function getUsername() {
      return $this->username;
  }

  function getPassword() {
      return $this->password;
  }

  static function setTable($table) {
         self::$table = $table;
     }

  function setId($id) {
      $this->id = $id;
  }

  function setUsername($username) {
      $this->username = $username;
  }

  function setPassword($password) {
      $this->password = $password;
  }


}
