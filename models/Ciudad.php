<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ciudad extends Model {

  protected static $table = "Ciudad";

  private $id;
  private $nombre;
  private $pais_id;

  private $has_one = array(
    'Pais'=>array(
        'class'=>'Pais',
        'join_as'=>'Pais_id',
        'join_with'=>'id'
        )
    );

private $has_many = array(
    'Teatros'=>array(
        'class'=>'Teatro',
        'my_key'=>'id',
        'other_key'=>'id',
        'join_as'=>'ciudad_id',
        'join_with'=>'id'
        )
    );


  function __construct($id, $nombre,$pais_id) {
      $this->id = $id;
      $this->nombre = $nombre;
      $this->pais_id=$pais_id;

  }


  public function getMyVars(){
      return get_object_vars($this);
  }

  static function getTable() {
      return self::$table;
  }
  function getId() {
      return $this->id;
  }

  function getNombre() {
      return $this->nombre;
  }

  function getHas_one() {
    return $this->has_one;
}

function getHas_many() {
    return $this->has_many;
}


  function setId($id) {
      $this->id = $id;
  }

  function setNombre($nombre) {
      $this->nombre = $nombre;
  }

  function getPais_id() {
      return $this->pais_id;
  }

  function setPais_id($pais_id) {
      $this->pais_id = $pais_id;
  }


  function setHas_one($has_one) {
      $this->has_one = $has_one;
  }

  function setHas_many($has_many) {
      $this->has_many = $has_many;
  }

  static function setTable($table) {
         self::$table = $table;
     }
}
