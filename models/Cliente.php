<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Cliente extends Model {

  protected static $table = "Cliente";

  private $id;
  private $nombre;
  private $contrasena;
  private $identificacion;
  private $tipo_identificacion_id;
  private $email;
  private $fechaNacimiento;
  private $telefono;


  function __construct($id, $nombre,$contrasena,$identificacion,$tipo_identificacion_id,$email,$fechaNacimiento,$telefono) {
      $this->id = $id;
      $this->nombre = $nombre;
      $this->contrasena = $contrasena;
      $this->identificacion = $identificacion;
      $this->tipo_identificacion_id = $tipo_identificacion_id;
      $this->email = $email;
      $this->fechaNacimiento = $fechaNacimiento;
      $this->telefono = $telefono;

  }


  public function getMyVars(){
      return get_object_vars($this);
  }

  static function getTable() {
      return self::$table;
  }
  function getId() {
      return $this->id;
  }

  function getNombre() {
      return $this->nombre;
  }

  function getContrasena() {
      return $this->contrasena;
  }

  function getIdentificacion() {
      return $this->identificacion;
  }

  function getTipo_identificacion_id() {
      return $this->tipo_identificacion_id;
  }

  function getEmail() {
      return $this->email;
  }

  function getFechaNacimiento() {
    return $this->fechaNacimiento;
}

function getTelefono() {
    return $this->telefono;
}


  function setId($id) {
      $this->id = $id;
  }

  function setNombre($nombre) {
      $this->nombre = $nombre;
  }

  function setContrasena($contrasena) {
      $this->contrasena = $contrasena;
  }

  function setIdentificacion($identificacion) {
      $this->identificacion = $identificacion;
  }

  function setTipo_identificacion_id($tipo_identificacion_id) {
      $this->tipo_identificacion_id = $tipo_identificacion_id;
  }

  function setEmail($email) {
      $this->email = $email;
  }

  function setFechaNacimiento($fechaNacimiento) {
    $this->fechaNacimiento = $fechaNacimiento;
}

function setTelefono($telefono) {
    $this->telefono = $telefono;
}

  static function setTable($table) {
         self::$table = $table;
     }

}
