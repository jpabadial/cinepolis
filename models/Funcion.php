<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Funcion extends Model {

  protected static $table = "Funcion";

  private $id;
  private $hora;
  private $fecha;
  private $Sala_id;
  private $Pelicula_id;

    private $has_one = array(
      'Pelicula'=>array(
          'class'=>'Pelicula',
          'join_as'=>'Pelicula_id',
          'join_with'=>'id'
          ),
      'Sala'=>array(
          'class'=>'Sala',
          'join_as'=>'Sala_id',
          'join_with'=>'id'
          )
      );

  private $has_many = array(
      'Reservas'=>array(
          'class'=>'Reserva',
          'my_key'=>'id',
          'other_key'=>'id',
          'join_other_as'=>'Funcion_id',
          'join_with'=>'id'
          )
      );


      function __construct($id, $hora, $fecha, $Sala_id, $Pelicula_id) {
          $this->id = $id;
          $this->hora = $hora;
          $this->fecha = $fecha;
          $this->Sala_id = $Sala_id;
          $this->Pelicula_id = $Pelicula_id;
      }


  public function getMyVars(){
      return get_object_vars($this);
  }


  static function getTable() {
      return self::$table;
  }

  function getId() {
      return $this->id;
  }

  function getHora() {
      return $this->hora;
  }

  function getFecha() {
      return $this->fecha;
  }

  function getSala_id() {
      return $this->Sala_id;
  }

  function getPelicula_id() {
      return $this->Pelicula_id;
  }

  function getHas_one() {
      return $this->has_one;
  }

  function getHas_many() {
      return $this->has_many;
  }

  static function setTable($table) {
      self::$table = $table;
  }

  function setId($id) {
      $this->id = $id;
  }

  function setHora($hora) {
      $this->hora = $hora;
  }

  function setFecha($fecha) {
      $this->fecha = $fecha;
  }

  function setSala_id($Sala_id) {
      $this->Sala_id = $Sala_id;
  }

  function setPelicula_id($Pelicula_id) {
      $this->Pelicula_id = $Pelicula_id;
  }

  function setHas_one($has_one) {
      $this->has_one = $has_one;
  }

  function setHas_many($has_many) {
      $this->has_many = $has_many;
  }


}
