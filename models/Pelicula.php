<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Pelicula extends Model {

  protected static $table = "Pelicula";

  private $id;
  private $titulo;
  private $subtitulada;
  private $poster;
  private $fecha_estreno;
  private $Categoria_id;
  private $Sinopsis;

  private $has_one = array(
      'Categoria'=>array(
          'class'=>'CategoriaPelicula',
          'join_as'=>'Categoria_id',
          'join_with'=>'id'
          )
      );

  private $has_many = array(
      'Funciones'=>array(
          'class'=>'Funcion',
          'my_key'=>'id',
          'other_key'=>'id',
          'join_other_as'=>'pelicula_id',
          'join_self_as'=>'id'
          )
      );


      function __construct($id, $titulo, $subtitulada, $poster, $fecha_estreno, $Categoria_id,$Sinopsis) {
          $this->id = $id;
          $this->titulo = $titulo;
          $this->subtitulada = $subtitulada;
          $this->poster = $poster;
          $this->fecha_estreno = $fecha_estreno;
          $this->Categoria_id = $Categoria_id;
          $this->Sinopsis = $Sinopsis;
      }

      public function getMyVars(){
      return get_object_vars($this);
  }


  static function getTable() {
      return self::$table;
  }

  function getId() {
      return $this->id;
  }

  function getTitulo() {
      return $this->titulo;
  }

  function getSubtitulada() {
      return $this->subtitulada;
  }

  function getPoster() {
      return $this->poster;
  }

  function getFecha_estreno() {
      return $this->fecha_estreno;
  }

  function getCategoria_id() {
      return $this->Categoria_id;
  }

  function getSinopsis() {
      return $this->Sinopsis;
  }

  function getHas_one() {
      return $this->has_one;
  }

  function getHas_many() {
      return $this->has_many;
  }

  static function setTable($table) {
      self::$table = $table;
  }

  function setId($id) {
      $this->id = $id;
  }

  function setTitulo($titulo) {
      $this->titulo = $titulo;
  }

  function setSubtitulada($subtitulada) {
      $this->subtitulada = $subtitulada;
  }

  function setPoster($poster) {
      $this->poster = $poster;
  }

  function setFecha_estreno($fecha_estreno) {
      $this->fecha_estreno = $fecha_estreno;
  }

  function setCategoria_id($Categoria_id) {
      $this->Categoria_id = $Categoria_id;
  }

  function setSinopsis($Sinopsis) {
      $this->Sinopsis = $Sinopsis;
  }

  function setHas_one($has_one) {
      $this->has_one = $has_one;
  }

  function setHas_many($has_many) {
      $this->has_many = $has_many;
  }



}
