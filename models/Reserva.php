<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Reserva extends Model {

  protected static $table = "Reserva";

  private $id;
  private $forma_de_pago;
  private $estado_pago;
  private $precio;
  private $Cliente_id;
  private $Funcion_id;

    private $has_many = array(
      'Sillas'=>array(
          'class'=>'Silla',
          'my_key'=>'id',
          'other_key'=>'id',
          'join_self_as'=>'Reserva_id_reserva',
          'join_as'=>'Silla_idSilla',
          'join_table'=>'Reserva_has_Silla'
          )
      );


      private $has_one = array(
     'Cliente'=>array(
         'class'=>'Cliente',
         'join_as'=>'Cliente_id',
         'join_with'=>'id'
         ),
     'Funcion'=>array(
         'class'=>'Funcion',
         'join_as'=>'Funcion_id',
         'join_with'=>'id'
         )

     );

     function __construct($id, $forma_de_pago, $estado_pago, $precio, $Cliente_id, $Funcion_id) {
         $this->id = $id;
         $this->forma_de_pago = $forma_de_pago;
         $this->estado_pago = $estado_pago;
         $this->precio = $precio;
         $this->Cliente_id = $Cliente_id;
         $this->Funcion_id = $Funcion_id;
     }

  public function getMyVars(){
      return get_object_vars($this);
  }

  static function getTable() {
      return self::$table;
  }

  function getId() {
      return $this->id;
  }

  function getForma_de_pago() {
      return $this->forma_de_pago;
  }

  function getEstado_pago() {
      return $this->estado_pago;
  }

  function getPrecio() {
      return $this->precio;
  }

  function getCliente_id() {
      return $this->Cliente_id;
  }

  function getFuncion_id() {
      return $this->Funcion_id;
  }

  function getHas_many() {
      return $this->has_many;
  }

  function getHas_one() {
      return $this->has_one;
  }

  static function setTable($table) {
      self::$table = $table;
  }

  function setId($id) {
      $this->id = $id;
  }

  function setForma_de_pago($forma_de_pago) {
      $this->forma_de_pago = $forma_de_pago;
  }

  function setEstado_pago($estado_pago) {
      $this->estado_pago = $estado_pago;
  }

  function setPrecio($precio) {
      $this->precio = $precio;
  }

  function setCliente_id($Cliente_id) {
      $this->Cliente_id = $Cliente_id;
  }

  function setFuncion_id($Funcion_id) {
      $this->Funcion_id = $Funcion_id;
  }

  function setHas_many($has_many) {
      $this->has_many = $has_many;
  }

  function setHas_one($has_one) {
      $this->has_one = $has_one;
  }

}
