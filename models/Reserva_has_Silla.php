<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Reserva_has_Silla extends Model {

  protected static $table = "Reserva_has_Silla";

  private $Reserva_id_reserva;
  private $Silla_idSilla;

  private $has_one = array(
     'Silla'=>array(
         'class'=>'Silla',
         'join_as'=>'Silla_idSilla',
         'join_with'=>'id'
         ),
     'Reserva'=>array(
         'class'=>'Reserva',
         'join_as'=>'Reserva_id_reserva',
         'join_with'=>'id'
         )

     );


     function __construct($Reserva_id_reserva, $Silla_idSilla) {
         $this->Reserva_id_reserva = $Reserva_id_reserva;
         $this->Silla_idSilla = $Silla_idSilla;
     }

     public function getMyVars(){
      return get_object_vars($this);
  }


  static function getTable() {
      return self::$table;
  }

  function getReserva_id_reserva() {
      return $this->Reserva_id_reserva;
  }

  function getSilla_idSilla() {
      return $this->Silla_idSilla;
  }

  function getHas_one() {
      return $this->has_one;
  }

  static function setTable($table) {
      self::$table = $table;
  }

  function setReserva_id_reserva($Reserva_id_reserva) {
      $this->Reserva_id_reserva = $Reserva_id_reserva;
  }

  function setSilla_idSilla($Silla_idSilla) {
      $this->Silla_idSilla = $Silla_idSilla;
  }

  function setHas_one($has_one) {
      $this->has_one = $has_one;
  }




}
