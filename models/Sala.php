<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Sala extends Model {

  protected static $table = "Sala";

  private $id;
  private $numero;
  private $capacidad;
  private $Teatro_id;
  private $Tipo_id;

  private $has_one = array(
    'TipoSala'=>array(
        'class'=>'TipoSala',
        'join_as'=>'tipo_id',
        'join_with'=>'id'
        ),
    'Teatro'=>array(
        'class'=>'Teatro',
        'join_as'=>'teatro_id',
        'join_with'=>'id'
        )
    );



  function __construct(  $id,$numero,$capacidad,$Teatro_id,$Tipo_id) {
      $this->id = $id;
      $this->numero = $numero;

      $this->capacidad = $capacidad;
      $this->Tipo_id= $Tipo_id;
      $this->Teatro_id = $Teatro_id;


  }


  public function getMyVars(){
      return get_object_vars($this);
  }

  static function getTable() {
      return self::$table;
  }


    static function setTable($table) {
        self::$table = $table;
    }
  function getId() {
      return $this->id;
  }

  function getNumero() {
      return $this->numero;
  }

  function getCapacidad() {
      return $this->capacidad;
  }

  function getTeatro_id() {
      return $this->Teatro_id;
  }

  function getTipo_id() {
      return $this->Tipo_id;
  }


  function getHas_one() {
      return $this->has_one;
  }

  function getHas_many() {
      return $this->has_many;
  }


  function setId($id) {
      $this->id = $id;
  }

  function setNumero($numero) {
      $this->numero = $numero;
  }

  function setCapacidad($capacidad) {
      $this->capacidad = $capacidad;
  }

  function setTeatro_id($Teatro_id) {
      $this->Teatro_id = $Teatro_id;
  }

  function setTipo_id($Tipo_id) {
      $this->Tipo_id = $Tipo_id;
  }

  function setHas_one($has_one) {
    $this->has_one = $has_one;
}

function setHas_many($has_many) {
    $this->has_many = $has_many;
}



}
