<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Silla extends Model {

  protected static $table = "Silla";

  private $id;
  private $numero;
  private $fila;
  private $estado;
  private $Sala_id;

  private $has_one = array(
    'Sala'=>array(
        'class'=>'Sala',
        'join_as'=>'sala_id',
        'join_with'=>'id'
        )
    );

private $has_many = array(
    'Reservas'=>array(
        'class'=>'Reserva',
        'my_key'=>'id',
        'other_key'=>'id',
        'join_other_as'=>'silla_id_silla',
        'join_self_as'=>'reserva_id_reserva',
        'join_table'=>'Reserva_has_Silla'
        )
    );

  function __construct($id, $numero, $fila, $estado, $Sala_id) {
      $this->id = $id;
      $this->numero = $numero;
      $this->fila = $fila;
      $this->estado = $estado;
      $this->Sala_id = $Sala_id;
  }

    public function getMyVars(){
      return get_object_vars($this);
  }

  static function getTable() {
      return self::$table;
  }

  function getId() {
      return $this->id;
  }

  function getNumero() {
      return $this->numero;
  }

  function getFila() {
      return $this->fila;
  }

  function getEstado() {
      return $this->estado;
  }

  function getSala_id() {
      return $this->Sala_id;
  }

  function getHas_one() {
      return $this->has_one;
  }

  function getHas_many() {
      return $this->has_many;
  }
  static function setTable($table) {
      self::$table = $table;
  }

  function setId($id) {
      $this->id = $id;
  }

  function setNumero($numero) {
      $this->numero = $numero;
  }

  function setFila($fila) {
      $this->fila = $fila;
  }

  function setEstado($estado) {
      $this->estado = $estado;
  }

  function setSala_id($Sala_id) {
      $this->Sala_id = $Sala_id;
  }

  function setHas_one($has_one) {
    $this->has_one = $has_one;
}

function setHas_many($has_many) {
    $this->has_many = $has_many;
}


}
