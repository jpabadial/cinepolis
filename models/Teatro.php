<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Teatro extends Model {

  protected static $table = "Teatro";

  private $id;
  private $nombre;
 private $estado;
  private $telefono;
  private $direccion;
  private $Ciudad_id;

  private $has_one = array(
    'Ciudad'=>array(
        'class'=>'Ciudad',
        'join_as'=>'ciudad_id',
        'join_with'=>'id'
        )
    );

private $has_many = array(
    'Salas'=>array(
        'class'=>'Sala',
        'my_key'=>'id',
        'other_key'=>'id',
        'join_as'=>'teatro_id',
        'join_with'=>'id'
        )
    );


  function __construct( $id,$nombre,$estado,$telefono,$direccion,$Ciudad_id) {
      $this->id = $id;
      $this->nombre = $nombre;
      $this->estado = $estado;
      $this->telefono = $telefono;
      $this->direccion = $direccion;
      $this->Ciudad_id = $Ciudad_id;

  }


  public function getMyVars(){
      return get_object_vars($this);
  }

  static function getTable() {
      return self::$table;
  }
  function getId() {
      return $this->id;
  }

  function getNombre() {
      return $this->nombre;
  }

  function getEstado() {
      return $this->estado;
  }

  function getTelefono() {
      return $this->telefono;
  }

  function getDireccion() {
      return $this->direccion;
  }

  function getCiudad_id() {
      return $this->Ciudad_id;
  }

  function getHas_one() {
    return $this->has_one;
}

function getHas_many() {
    return $this->has_many;
}

  function setId($id) {
      $this->id = $id;
  }

  function setNombre($nombre) {
      $this->nombre = $nombre;
  }

  function setEstado($estado) {
      $this->estado = $estado;
  }

  function setTelefono($telefono) {
      $this->telefono = $telefono;
  }

  function setDireccion($direccion) {
      $this->direccion = $direccion;
  }

  function setCiudad_id($Ciudad_id) {
      $this->Ciudad_id = $Ciudad_id;
  }


  function setHas_one($has_one) {
      $this->has_one = $has_one;
  }

  function setHas_many($has_many) {
      $this->has_many = $has_many;
  }

  static function setTable($table) {
         self::$table = $table;
     }

}
